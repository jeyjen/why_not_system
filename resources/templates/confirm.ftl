<html>
    <head>
        <meta charset=utf-8>
        <meta name=format-detection content="telephone=no">
        <meta name=msapplication-tap-highlight content=no>
        <meta name=viewport content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width">
        <title>Profile</title>
        <link rel="shortcut icon" href=./favicon.ico type=image/x-icon>
    </head>
    <body>
        <div id=app style="margin-top: 100px; text-align: center; font-size:150%">${message}</div>
    </body>
</html>