package demo

import api.Mutation
import com.coxautodev.graphql.tools.SchemaParser
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import demo.api.*
import whynot.api.resolver.*
import whynot.entity.*
import demo.provider.*

import demo.util.Graphql_endpoint
import demo.util.Util
import graphql.GraphQL
import graphql.language.*
import graphql.schema.Coercing
import graphql.schema.GraphQLScalarType
import graphql.execution.batched.BatchedExecutionStrategy


class App_context(val host: String, val port: String, val domain: String = ""){
    val concept_pvd = ConceptPvd(host, port)
    val ru_pvd = Ru_pvd()
    val user_pvd = Person_pvd(host, port)
    val email_pvd = Email_pvd(domain)
    val service = Service()
}

class Schema(ctx: App_context): Graphql_endpoint.Graphql_provider() {
    private val SDL_FILE = "schema.graphqls"

    private val schema = SchemaParser.newParser()
            .schemaString(SDL())
            .resolvers(Query(ctx), Mutation(ctx), Word_rsr(ctx), WN_account_rsr(ctx), Person_rsr(ctx))
            .scalars(Object, Collection, Long, JNode)
            .dictionary(Word::class, Phrase::class, Example::class, Detail::class, Achievement::class, Repetition::class)//
            .build()
            .makeExecutableSchema()

    override val graphQL = GraphQL
            .newGraphQL(schema)
            .queryExecutionStrategy(BatchedExecutionStrategy())
            .build()
    override fun SDL(): String {
        return java.io.BufferedReader(java.io.InputStreamReader(
                object : Any() {}.javaClass.classLoader.getResourceAsStream(SDL_FILE) ?: throw java.io.FileNotFoundException("classpath:$SDL_FILE")
        )).readText()
    }
}

fun <T, T2> Coercing<T, T2>.parse_value(value: Any?): Any?{
    when(value){
        is ObjectValue -> {
            val obj = Util.om.createObjectNode()
            for (f in value.objectFields){
                obj.set(f.name, Util.om.valueToTree(this.parse_value(f.value)))
            }
            return obj
        }
        is ArrayValue -> {
            val arr = Util.om.createArrayNode()
            for(v in value.values){
                arr.add(Util.om.valueToTree<JsonNode>(parse_value(v)))
            }
            return arr
        }
        is StringValue -> { return value.value }
        is BooleanValue -> { return value.isValue }
        is EnumValue -> { return value }
        is FloatValue -> { return value.value }
        is IntValue -> { return value.value}
        is NullValue -> { return null}
    }
    return  null
}
val JNode = GraphQLScalarType("JNode", "Object of any type", object : Coercing<JsonNode, Any> {
    override fun parseLiteral(input: Any?): JsonNode {
        val res = parse_value(input)
        if(res is JsonNode){
            return res
        }
        else{
            throw Exception("Can not create instance of JNode")
        }
    }

    override fun parseValue(input: Any?): ObjectNode {

        return Util.om.valueToTree(input)
    }

    override fun serialize(dataFetcherResult: Any?): Any {
        return dataFetcherResult?: ""
    }
})
val Object = GraphQLScalarType("Object", "Object of any type", object : Coercing<ObjectNode, Any> {
    override fun parseLiteral(input: Any?): ObjectNode {
        if(input is ObjectValue){
            val res = parse_value(input)
            if(res is ObjectNode){
                return res
            }
            throw Exception("Can not create object parse_call value")
        }
        else{
            throw Exception("Value is not array")
        }
    }

    override fun parseValue(input: Any?): ObjectNode {
        return return Util.om.valueToTree(input)//To change body of created functions use File | Settings | File Templates.
    }

    override fun serialize(dataFetcherResult: Any?): Any {
        return dataFetcherResult?: ""
    }
})
val Collection = GraphQLScalarType("Collection", "Collection of any types", object : Coercing<ArrayNode, Any> {
    val _om = ObjectMapper()
    override fun parseLiteral(input: Any?): ArrayNode {
        val arr = _om.createArrayNode()
        if(input is ArrayValue){
            for(v in input.values){
                arr.add(_om.valueToTree<JsonNode>(parse_value(v)))
            }
        }
        else{
            throw Exception("Value is not object")
        }
        return arr
    }

    override fun parseValue(input: Any?): ArrayNode {
        val arr = _om.createArrayNode()
//        if(input is ArrayValue){
//            for(v in input.values){
//                arr.add(_om.valueToTree<JsonNode>(parse_value(v)))
//            }
//        }
//        else{
//            throw Exception("Value is not object")
//        }

        return _om.valueToTree<ArrayNode>(input)
        //return ObjectMapper().createArrayNode()//To change body of created functions use File | Settings | File Templates.
    }

    override fun serialize(dataFetcherResult: Any?): Any {
        return dataFetcherResult?: ""
    }
})
val Long = GraphQLScalarType("Long", "Long", object : Coercing<Long, Any> {

    override fun parseLiteral(input: Any?): Long {
        return input as Long
    }

    override fun parseValue(input: Any?): Long {
        return input as Long
    }

    override fun serialize(dataFetcherResult: Any?): Any {
        return dataFetcherResult?: ""
    }
})



