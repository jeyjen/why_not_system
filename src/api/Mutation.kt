package api

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.fasterxml.jackson.databind.node.ObjectNode
import demo.App_context
import demo.Schema
import whynot.entity.Practice
import demo.util.Request
import demo.util.Util
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test


class Mutation(val ctx: App_context): GraphQLMutationResolver {
    fun concepts(segments: List<Segment>): Int{
        return runBlocking { ctx.concept_pvd.concept_save(segments) }
    }
    fun consonance(word: String, consonance: List<String>): Int{
        return runBlocking { ctx.concept_pvd.consonance_save(word, consonance) }
    }
    fun test(m_student: String, practices: List<Practice>): Int{
        return runBlocking { ctx.concept_pvd.test_save(m_student, practices) }
    }
    fun repetition(m_student: String, practices: List<Practice>): Int{
        return runBlocking { ctx.concept_pvd.repetition_save(m_student, practices) }
    }
    fun to_work(m_student: String, m_concept:String, image: String = "", include: Boolean = true): Int{
        return runBlocking { ctx.concept_pvd.to_work(m_student, m_concept, image, include) }
    }
    fun change_password(account_id: String, old_password: String, new_password: String): Int{
        return runBlocking { ctx.user_pvd.change_password(account_id, old_password, new_password) }
    }
    fun change_email(account_id: String, email: String): Int{
        return runBlocking { ctx.user_pvd.change_email(account_id, email) }
    }
    fun change_purpose(account_id: String, purpose: String): Int{
        return runBlocking { ctx.user_pvd.change_purpose(account_id, purpose) }
    }
}
data class Segment(var type: String = "", var unit: ObjectNode = Util.om.createObjectNode() )

class Querty_tests {
    val ctx = App_context("http://localhost", "5984")
    val s = Schema(ctx)
    @Test
    fun change_purpose(){
        print(s.execute(Request("""
            mutation ss{
                change_purpose(account_id:"2be236cc70b9fb75e2aa7e3d2a7d0356", purpose: "кайфовать с новыми друзьями в Канаде")
            }""".trimIndent())).toSpecification())
    }
    @Test
    fun change_email(){
        print(s.execute(Request("""
            mutation ss{
                change_email(account_id:"2be236cc70b9fb75e2aa7e3d2a7d0356", email: "novikov.eugeniy2@gmail.com")
            }""".trimIndent())).toSpecification())
    }
    @Test
    fun change_password() {

        print(s.execute(Request("""
            mutation ss{
                change_password(account_id:"2be236cc70b9fb75e2aa7e3d2a7d0356", old_password: "111", new_password: "222")
            }""".trimIndent())).toSpecification())
    }
    @Test
    fun concept_save() {
        val s = Schema(ctx)
        print(s.execute(Request("""mutation ss{ concepts(segments:[{type: "Word", unit:{_id: "b32129a4062b8b28b84201e86f003387", ru:{mean:"⚠время после полудня!"}}}]) }""")).toSpecification())
    }
    @Test
    fun consonance() {
        val s = Schema(ctx)
        print(s.execute(Request("""mutation ss{ consonance(word: "table", consonance:["таблетка"]) }""")).toSpecification())
    }
    @Test
    fun to_work() {
        val s = Schema(ctx)
        print(s.execute(Request("""mutation ss{ to_work(m_student: "stud123", m_concept:"b32129a4062b8b28b84201e86f10f087", image:"lalaley", include: true) }""")).toSpecification())
    }
}