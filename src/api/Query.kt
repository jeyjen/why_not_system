package demo.api

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver
import com.fasterxml.jackson.databind.node.ObjectNode
import com.google.common.base.Stopwatch
import demo.App_context
import demo.Schema
import demo.api.resolver.ListListResolver
import whynot.entity.*
import demo.provider.*
import demo.util.Request
import graphql.execution.batched.Batched

import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test

class Query(val ctx: App_context): GraphQLQueryResolver {
    fun concepts(types: List<String>, levels: List<String>, kinds: List<String>, topics:List<String>, weight: Range, match: String): List<Any>{
        return runBlocking {  ctx.concept_pvd.concepts(types, levels, kinds, topics, weight, match)}
    }
    fun bundle(m_student: String, type: Bundle_type, topic: String = ""):List<Any>{
        return runBlocking {  ctx.concept_pvd.bundle(m_student, type, topic)}
    }

    fun report(m_student: String): ObjectNode {
        return runBlocking { ctx.concept_pvd.report(m_student) }
    }
    fun plan(m_student: String): ObjectNode {
        return runBlocking { ctx.concept_pvd.plan(m_student) }
    }
    fun consonance(transcription: String):List<String>{
        return runBlocking {  ctx.ru_pvd.consonance(transcription) }
    }
    fun ru_by_prefix(prefixes: List<String>):List<String>{
        return runBlocking {  ctx.ru_pvd.ru_by_prefix(prefixes) }
    }
    fun translate(lexeme: String, pos: String): List<Translate>{
        return  runBlocking { ctx.service.translate(lexeme, pos) }
    }
    fun wn_accounts(account_ids: List<String>, logins: List<String>): List<WN_account>{
        return runBlocking { ctx.user_pvd.wn_accounts(account_ids, logins) }
    }
}

class Querty_tests{
    val ctx = App_context("http://localhost", "5984")
    @Test
    fun wn_accounts(){
        val s = Schema(ctx)

        val q = """
            {
                wn_accounts(account_ids:["2be236cc70b9fb75e2aa7e3d2a4e2477"], logins: []){
                    id
                    person{
                        firstname,
                        lastname,
                        contacts{
                            value
                        }
                        wn_account{
                            login
                        }
                        wn_settings{
                            purpose
                        }
                    }
                }
            }"""
        val res = s.execute(Request(q))
        println(res.toSpecification())
    }
    @Test
    fun persons(){
        val s = Schema(ctx)

        val q = """
            {
                persons{id, firstname, contacts{type, value}}
            }"""
        val res = s.execute(Request(q))
        println(res.toSpecification())
    }


    @Test
    fun concepts(){
        val s = Schema(ctx)

        val q = """
            {
                concepts(types: ["Phrase", "Word"], levels: ["A1"], kinds: [], topics:[], weight:{from:1, to: 1001}, match: "table"){
                    ... on Word{spelling, examples{text}, consonance, ru_tr}
                    ... on Phrase{spelling, examples{text}}
                }
            }"""

//        for (i in 0..2) {
//            s.execute(Request(q))
//        }

        val timer = Stopwatch.createUnstarted()
        timer.start()
        val res = s.execute(Request(q))
        timer.stop()
        System.out.println(timer)
        println(res.toSpecification())
    }
    @Test
    fun concepts_with_achievment(){
        val s = Schema(ctx)

        val q = """
            {
                concepts(types: ["Word"], levels: ["A1"], kinds: [], topics:[], weight:{from:1, to: 1001}, match: "table"){
                    ... on Word{id,spelling,achievement(m_student: "stud123"){init_progress}}
                    ... on Phrase{spelling, examples{text}}
                }
            }"""

        //

        val timer = Stopwatch.createUnstarted()
        timer.start()
        val res = s.execute(Request(q))
        timer.stop()
        System.out.println(timer)
        println(res.toSpecification())
    }
    @Test
    fun ru_by_prefix(){
        val s = Schema(ctx)

        val q = """
            {
                ru_by_prefix(prefixes: ["кни", "кну"])
            }"""

        val res = s.execute(Request(q))
        println(res)
    }
    @Test
    fun translate(){
        val s = Schema(ctx)

        val q = """
            {
                translate(lexeme: "icon", pos: "noun"){
                    translate
                }
            }"""

        for (i in 0..2) {
            s.execute(Request(q))
        }

        val timer = Stopwatch.createUnstarted()
        timer.start()
        val res = s.execute(Request(q))
        timer.stop()
        System.out.println(timer)
        println(res.toSpecification())
    }
    @Test
    fun report(){
        val s = Schema(ctx)
        val q = """
            {
                report(m_student: "stud123")
            }
        """.trimIndent()

        var res = s.execute(Request(q))
        println(res)
    }
    @Test
    fun plan(){
        val s = Schema(ctx)
        val q = """
            {
                plan(m_student: "807803ae594d84510697fd1d2b004875")
            }
        """.trimIndent()

        var res = s.execute(Request(q))
        println(res)
    }
    @Test
    fun bundle(){
        val s = Schema(ctx)

        val q = """
            {
                bundle(m_student: "807803ae594d84510697fd1d2b004875", type: today, topic: ""){
                    ... on Word{spelling, examples{text}, consonance}
                    ... on Phrase{spelling, examples{text}}
                }
            }"""

//        for (i in 0..2) {
//            s.execute(Request(q))
//        }

        val timer = Stopwatch.createUnstarted()
        timer.start()
        val res = s.execute(Request(q))
        timer.stop()
        System.out.println(timer)
        println(res.toSpecification())
    }
}


