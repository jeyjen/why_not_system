package api.old

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.fasterxml.jackson.databind.ObjectMapper
import demo.api.couchdb.couchdb
import kotlinx.coroutines.experimental.runBlocking
import org.junit.experimental.categories.Category
import org.junit.Test as test
import org.junit.runners.Suite.SuiteClasses
import org.junit.experimental.categories.Categories.IncludeCategory
import org.junit.experimental.categories.Categories
import org.junit.runner.RunWith




class Query: GraphQLQueryResolver {
    fun view(design: String, name: String, group: Boolean=false, include_docs: Boolean=false): api.old.View {
        // запрос конвертера с названием view
//        return runBlocking {
//            val c = couchdb("demo", "http://localhost:5984")
//            val res = c.view("app", "nn")
//            val om = ObjectMapper()
//            om.treeToValue(res, View::class.java)
//
//        }
        return api.old.View()
    }
    // передвать опции поиска
    // для каждого из объектов определться поля которые планируется вернуть
    // нужна операция объединения
    fun find(): List<Any>{
        var w = api.old.Word()
        w.spelling = "hello"
        var p = api.old.Phrase()
        p.spelling = "asd"
        var r = ArrayList<api.old.Word>()
//        r.add(w)
//        r.add(p)
        return listOf(w, p) //Search_result()
    }

    @Category(api.old.SlowTests::class)
    @test
    fun create_test_scheme(){

        val res = api.old.Query().find()
    }
    @test
    fun create_test_scheme2(){

        val res = api.old.Query().find()
    }
}



class Search_result : ArrayList<Any>{
    constructor():super(100)
}

class View(){
    var rows: List<api.old.Row>? = null
    var total_rows: Int? = null
    var offset: Int? = null
}
class Row(){
    var key: String? = null
    var value: Any? = null
    var doc: Any? = null
    var id: String? = null
}

class Word(){
    var _id = ""
    var _rev = ""
    var type = ""
    var spelling = ""
    var irregular_forms = mutableListOf<String>()
    var senses = mutableListOf<api.old.Sense>()
}

class Phrase{
    var _id = ""
    var _rev = ""
    var type = ""
    var base = ""
    var spelling = ""
    var senses = mutableListOf<api.old.Sense>()
}

class Phrase_verb{
    var _id = ""
    var _rev = ""
    var type = ""
    var base = ""
    var spelling = ""
    var senses = mutableListOf<api.old.Sense>()
}

class Sense{
    var pos = mutableListOf<String>()
    var level = ""
    var mean = ""
    var def = ""
    var grams = mutableListOf<String>()
    var examples = mutableListOf<String>()
    var topics = mutableListOf<String>()
}

public interface FastTests { /* category marker */ }
public interface SlowTests { /* category marker */ }

@RunWith(Categories::class)
//@IncludeCategory(SlowTests::class)
@SuiteClasses(api.old.Query::class) // Note that Categories is a kind of Suite
class SlowTestSuite{}

