package whynot.api.resolver

import com.coxautodev.graphql.tools.GraphQLResolver
import demo.App_context
import demo.api.resolver.ListListResolver
import graphql.execution.batched.Batched
import kotlinx.coroutines.experimental.runBlocking
import whynot.entity.*

class Person_rsr(val ctx: App_context): GraphQLResolver<Person>, ListListResolver<Contact>(){ //ListListResolver<Example>
    @Batched
    fun contacts(persons: List<Person> ): List<List<Contact?>>{
        val person_ids = persons.map {it.id}

        val cs = runBlocking {  ctx.user_pvd.contacts(person_ids = person_ids)}
        val map = mutableMapOf<String, MutableList<Contact?>>()
        cs.forEach {
            val v = map.get(it.m_person)
            if(v == null){
                map.put(it.m_person, mutableListOf(it))
            }
            else{
                v.add(it)
            }
        }
        return persons.map {map.get(it.id)?: mutableListOf() }
    }
    @Batched
    fun wn_accounts(persons: List<Person>): List<List<WN_account?>>{
        val person_ids = persons.map { it.id }
        val acs = runBlocking {  ctx.user_pvd.wn_accounts(person_ids = person_ids) }
        val map = mutableMapOf<String, MutableList<WN_account>>()
        acs.forEach {
            val v = map.get(it.m_person)
            if(v == null){
                map.put(it.m_person, mutableListOf(it))
            }
            else{
                v.add(it)
            }
        }
        return persons.map {map.get(it.id)?: mutableListOf()  }
    }
    @Batched
    fun wn_setting(persons: List<Person>): List<WN_setting?>{
        val person_ids = persons.map { it.id }
        val settings = runBlocking {  ctx.user_pvd.wn_settings(person_ids = person_ids) }
        val map = mutableMapOf<String, WN_setting>()
        settings.forEach { map.put(it.m_person, it) }
        return persons.map { map.get(it.id)?: null }
    }

}