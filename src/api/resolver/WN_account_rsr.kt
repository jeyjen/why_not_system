package whynot.api.resolver

import com.coxautodev.graphql.tools.GraphQLResolver
import demo.App_context
import demo.Schema
import demo.util.Request
import graphql.execution.batched.Batched
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test
import whynot.entity.Person
import whynot.entity.WN_account

class WN_account_rsr(val ctx: App_context): GraphQLResolver<WN_account> {
    @Batched
    fun person(accounts: List<WN_account>): List<Person?>{

        val ids = accounts.map { it.id }
        val ps = runBlocking { ctx.user_pvd.persons(account_ids = ids)}
        val map = mutableMapOf<String, Person>()
        ps.forEach { map.put(it.id, it) }
        return accounts.map { map.get(it.m_person) }
    }
}

class WN_account_rsr_tests {
    val ctx = App_context("http://localhost", "5984")
    val s = Schema(ctx)
    @Test
    fun wn_accounts() {


        val q = """
            {
                wn_accounts(account_ids:["2be236cc70b9fb75e2aa7e3d2a7d0356"], logins: []){
                    id
                    person{
                        firstname,
                        lastname,
                        contacts{
                            value
                        }
                        wn_accounts{
                            login
                        }
                        wn_settings{
                            purpose
                        }
                    }
                }
            }"""
        val res = s.execute(Request(q))
        println(res.toSpecification())
    }

    @Test
    fun persons() {

        val q = """
            {
                persons{id, firstname, contacts{type, value}}
            }"""
        val res = s.execute(Request(q))
        println(res.toSpecification())
    }
}