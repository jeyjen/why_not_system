package whynot.api.resolver

import com.coxautodev.graphql.tools.GraphQLResolver
import demo.App_context
import graphql.execution.batched.Batched
import kotlinx.coroutines.experimental.runBlocking
import whynot.entity.Achievement
import whynot.entity.Example
import whynot.entity.Word

class Word_rsr(val ctx: App_context): GraphQLResolver<Word> {
    @Batched
    fun examples(words: List<Word>): List<List<Example>>{
        return runBlocking {  ctx.concept_pvd.word_examples(words)}
    }
    @Batched
    fun consonance(words: List<Word>): List<List<String>>{
        return runBlocking {  ctx.concept_pvd.word_consonance(words)}
    }
    @Batched
    fun achievement(words: List<Word>, m_student: String): List<Achievement?>{
        return return runBlocking {  ctx.concept_pvd.achievement(words, m_student)}
    }
    @Batched
    fun ru_tr(words: List<Word>): List<String>{
        return return runBlocking {  ctx.concept_pvd.ru_tr(words)}
    }
}