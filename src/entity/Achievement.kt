package whynot.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class Achievement{
    @JsonProperty("_id")
    var id: String = ""
    var m_student: String = ""
    var m_concept: String = ""
    var repetitions: List<Repetition>? = null
    var init_progress: Int = 0
    var in_process: Boolean = false
    var image: String = ""
}
@JsonIgnoreProperties(ignoreUnknown = true)
class Repetition{
    var datetime = ""
    var result: Int = 0
}

class Practice{
    var m_concept: String = ""
    var result: Int = 0
}