package whynot.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSetter

@JsonIgnoreProperties(ignoreUnknown = true)
class Word{
    @JsonSetter("_id")
    var id: String = ""
    var type: String? = null
    var spelling: String? = null
    var tr: String? = null
    var morphs: MutableList<String>? = null
    var lemma: MutableList<String>? = null
    var en: Detail? = null
    var ru: Detail? = null
    var level: String? = null
    var weight: Int = 1
    var topic: String? = null
    var base: String? = null
    var m_examples: MutableList<String>? = null
    var examples: MutableList<Example>? = null
    var consonance: MutableList<String>? = null
}
enum class Bundle_type{now, today, wrongs}
@JsonIgnoreProperties(ignoreUnknown = true)
data class Range(var from: Float, var to: Float)
@JsonIgnoreProperties(ignoreUnknown = true)
class Phrase{
    @JsonSetter("_id")
    var id: String = ""
    var type: String? = null
    var spelling: String? = null
    var lemma: MutableList<String>? = null
    var en: Detail? = null
    var ru: Detail? = null
    var level: String? = null
    var weight: Int = 1
    var topic: String? = null
    var base: String? = null
    var m_examples: MutableList<String>? = null
    var examples: MutableList<Example>? = null
}
@JsonIgnoreProperties(ignoreUnknown = true)
data class Detail(
        var mean: String = "",
        var def: String = "")
@JsonIgnoreProperties(ignoreUnknown = true)
class Example {
    @JsonSetter("_id")
    var id: String = ""
    var text: String = ""
    var ru: String = ""
}

class Pattern{
    @JsonProperty("_id")
    val id = ""
    val structure = ""
    val description = ""
}

class Pattern_example{
    @JsonProperty("_id")
    val id = ""
    var m_pattern = ""
    var spelling = ""
    var translate = ""
    var hint = ""
}