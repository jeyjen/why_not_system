package whynot.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class Person{
    @JsonProperty("_id")
    var id = ""
    var firstname = ""
    var lastname = ""
    var middlename = ""
}

@JsonIgnoreProperties(ignoreUnknown = true)
class WN_account{
    @JsonProperty("_id")
    var id = ""
    var login = ""
    var password = ""
    var m_person = ""
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Contact{
    @JsonProperty("_id")
    var id = ""
    var type = Contact_type.none
    var value = ""
    var confirmed = false
    var confirm_value = ""
    var m_person = ""
}

enum class Contact_type{none, email, phone, vk, fb}

@JsonIgnoreProperties(ignoreUnknown = true)
class WN_setting{
    @JsonProperty("_id")
    var id = ""
    var purpose = ""
    var m_person = ""
}

@JsonIgnoreProperties(ignoreUnknown = true)
class User{
    @JsonProperty("_id")
    var id: String = ""
    var firstname: String = ""
    var lastname: String = ""
    var middlename: String = ""
    var phone: Contact = Contact()
    var email: Contact = Contact()
    var vk: Contact = Contact()
    var purpose: String = ""
    var secret: String = ""
}