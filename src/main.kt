package demo

import com.coxautodev.graphql.tools.SchemaParser
import com.fasterxml.jackson.databind.JsonNode
import java.io.File
import com.fasterxml.jackson.databind.ObjectMapper
import demo.util.Config
import demo.util.splitPair
import io.ktor.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.junit.Test as test

fun main(args: Array<String>){

    val arg = args.mapNotNull { it.splitPair('=') }.toMap()

    var db_host = arg.get("-db_host")?: "http://localhost"
    var db_port = arg.get("-db_port")?: "5984"
    var domain = arg.get("-domain")?: "http://localhost"
    Config.set(db_host, db_port, domain)

    val server = embeddedServer(
            Netty,
            port = arg.get("-port")?.toInt()?: 80,
            module = Application::service

    ).apply { start(wait = true) }
}



//data class HelloWorld(val hello: String)
//fun Application.mymodule() {
//    install(ContentNegotiation) {
//        jackson {}
//    }
//    routing {
//        post("/") {
//            val js = call.receive<JsonObject>()
//            val d = js.get("hello").asString ?: ""
//            println("SERVER: Message parse_call the client: $d")
//            call.respond(HelloWorld(hello = "response"))
//        }
//    }
//}

//fun Application.testableModule() {
//    intercept(ApplicationCallPipeline.Call) { call ->
//        if (call.Request.uri == "/")
//            call.respondText("Test String")
//    }
//}

//runBlocking {
//    val client = HttpClient(CIO) {
//        install(JsonFeature) {
//            serializer = GsonSerializer {
//                // .GsonBuilder
//                serializeNulls()
//                disableHtmlEscaping()
//            }
//        }
//    }
//
//    val message = client.post<HelloWorld> {
//        url("http://127.0.0.1:7878/")
//        contentType(ContentType.Application.Util)
//        body = HelloWorld(hello = "world")
//    }
//    println("CLIENT: Message parse_call the server: $message")
//        client.close()
//        server.stop(1L, 1L, TimeUnit.SECONDS)
//}


// добавить все функции view - запомнить порядок
// на каждый map_doc вызвать выполнение каждой этой функции

//fun retrieve (Request: Array<String>){
//    var data = create_report(Request)
//    val report_id = data.get("data").asText()
//    var status = ""
//    do {
//        var data = get_reports()
//        val reps = data.get("data")
//        for(n in reps){
//            if(n.get("ReportID").asText() == report_id){
//                status = n.get("StatusReport").asText()
//            }
//        }
//    }while (status != "Done")
//
//    data = get_report(report_id)
//    val ps = data.get("data")
//    for(ph in ps){
//        for(row in ph.get("SearchedWith")){
//            val ph = row.get("Phrase").asText()
//            val shows = row.get("Shows").asLong()
//        }
//        for(row in ph.get("SearchedAlso")){
//            val ph = row.get("Phrase").asText()
//            val shows = row.get("Shows").asLong()
//        }
//    }
//    // сформировать запрос на сапись в CouchDB
//
//    val d2 = get_reports()
//}

//fun create_report(Request: Array<String>): JsonNode{
//    var obj = ObjectMapper()
//    val root = obj.createObjectNode()
//    root.put("locale", "ru")
//    root.put("token", "AQAAAAAgWkwqAASIJkTKZzWU1UxKqVeWEUhO5GU")
//    root.put("method", "CreateNewWordstatReport")
//    val param = root.putObject("param")
//    val ps = param.putArray("Phrases")
//    for(i in Request){
//        ps.add(i)
//    }
//    param.putArray("GeoID")
//
//    val outputStream = ByteArrayOutputStream()
//    obj.writeValue(outputStream, root)
//    val res = outputStream.toString()
//    return Request(res)
//}
//fun get_reports(): JsonNode{
//    var obj = ObjectMapper()
//    val root = obj.createObjectNode()
//    root.put("locale", "ru")
//    root.put("token", "AQAAAAAgWkwqAASIJkTKZzWU1UxKqVeWEUhO5GU")
//    root.put("method", "GetWordstatReportList")
//    val outputStream = ByteArrayOutputStream()
//    obj.writeValue(outputStream, root)
//    val res = outputStream.toString()
//    return Request(res)
//}
//fun get_report(report_id: String): JsonNode{
//    var obj = ObjectMapper()
//    val root = obj.createObjectNode()
//    root.put("locale", "ru")
//    root.put("token", "AQAAAAAgWkwqAASIJkTKZzWU1UxKqVeWEUhO5GU")
//    root.put("method", "GetWordstatReport")
//    root.put("param", report_id)
//    val outputStream = ByteArrayOutputStream()
//    obj.writeValue(outputStream, root)
//    val res = outputStream.toString()
//    return Request(res)
//}
//fun delete_report(report_id: String){
//    var obj = ObjectMapper()
//    val root = obj.createObjectNode()
//    root.put("locale", "ru")
//    root.put("token", "AQAAAAAgWkwqAASIJkTKZzWU1UxKqVeWEUhO5GU")
//    root.put("method", "DeleteWordstatReport")
//    root.put("param", report_id)
//    val outputStream = ByteArrayOutputStream()
//    obj.writeValue(outputStream, root)
//    val res = outputStream.toString()
//    Request(res)
//}



//fun Request(t: String): JsonNode{
//    val r = "https://api-sandbox.direct.yandex.ru/v4/json/".httpPost()
//    r.body(t, UTF_8)
//    val resp = r.responseString()
//    val _om = ObjectMapper()
//    return  _om.readTree(resp.third.component1())
//}