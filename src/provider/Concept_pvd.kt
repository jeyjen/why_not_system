package demo.provider

import api.Segment
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.treeToValue
import com.google.common.base.Stopwatch
import demo.api.couchdb.couchdb
import whynot.entity.*
import demo.util.Util
import demo.util.clear_time
import demo.util.frt
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test
import kotlin.test.assertEquals

class ConceptPvd(host: String, port: String){
    val _conceptdb = couchdb("concepts", "${host}:${port}")
    val _traindb = couchdb("train", "${host}:${port}")

    val _conceptdb_types = setOf("Word", "Phrase", "Example")
    val _base_report = init_base_report()

    init{}

    fun init_base_report(): ObjectNode{
        val data =  runBlocking{_conceptdb.view("app", "level_type_topic").arguments(group = true).retrieve()}
        val rs = data.get("results").get(0).get("rows")

        val report = Util.om.createObjectNode()
        for(r in rs){
            val key = r.get("key")
            val value = r.get("value")
            //val key = r.get("key").joinToString("_"){it-> it.asText()}
            //_common_volume.put(key, Pair(v.get("weight").asInt(), v.get("count").asInt()))

            val level = key.get(0).asText()
            val type = key.get(1).asText()
            val topic = key.get(2).asText()



            var weight = value.get("weight").asInt()
            var count = value.get("count").asInt()

            var l = report?.get(level) as? ObjectNode
            if(l == null){
                l = Util.om.createObjectNode()
                report.set(level, l)
            }
            var t = l?.get(type) as? ObjectNode
            if(t == null){
                t = Util.om.createObjectNode()
                l?.set(type, t)
            }
            var tc = t?.get(topic) as? ObjectNode
            if(tc == null){
                tc = Util.om.createObjectNode()
                t?.set(topic, tc)
            }
            tc?.put("w", weight)
            tc?.put("c", count)
            if(!tc?.has("todo")!!){
                tc?.set("todo", Util.om.createObjectNode()?.put("w", 0)?.put("c", 0))
            }
            if(!tc?.has("doing")!!){
                tc?.set("doing", Util.om.createObjectNode()?.put("w", 0)?.put("c", 0))
            }
            if(!tc?.has("done")!!){
                tc?.set("done", Util.om.createObjectNode()?.put("w", 0)?.put("c", 0))
            }
            var u = tc?.get("udf") as? ObjectNode
            if(u == null){
                u = Util.om.createObjectNode()?.put("w", weight)?.put("c", count)
                tc?.set("udf", u)
            }
        }
        return report
    }

    suspend fun concepts(types: List<String>, levels: List<String>, kinds: List<String>, topics:List<String>, weigth: Range, match: String): List<Any>{
        var ts = types
        if(types.size == 0){
            ts = listOf("Phrase", "Word")
        }
        var ls = levels
        if(levels.size == 0){
            ls = listOf("A1", "A2", "B1", "B2", "C1", "C2")
        }
        val keys = Util.keys(ts, ls)

        val selector = Util.om.createObjectNode()

        if(topics.size > 0){
            val cond = Util.om.createObjectNode()
            for (i in topics){
                cond.put(i, "")
            }
            selector.set("topic", Util.om.createObjectNode().set("\$in", cond))
        }
        if(kinds.size > 0){
            val cond = Util.om.createObjectNode()
            for (i in kinds){
                cond.put(i, "")
            }
            selector.set("type", Util.om.createObjectNode().set("\$in", cond))
        }
        if(weigth.from >= 0 && weigth.to >= 0){
            val cond = Util.om.createObjectNode()
            cond.put("from", weigth.from)

            if(weigth.to >= 1001){
                cond.put("to", 10000)
            }
            else{
                cond.put("to", weigth.to)
            }
            selector.set("weight", Util.om.createObjectNode().set("\$between", cond))
        }
        if(!match.isNullOrEmpty()){
            selector.set("spelling", Util.om.createObjectNode().put("\$regex", ".*${match}.*"))
        }

        val sort = Util.om.createObjectNode()
        sort.put("weight", "desc")

        val data =  _conceptdb.list("app", "filter", "v_type_level",
                    include_docs = true,
                    reduce = false,
                    selector = selector,
                    sort = sort,
                    keys = Util.om.valueToTree(keys)
            )

        var concepts = mutableListOf<Any>()

        val docs = data.get("docs")
        for(doc in docs){
            val t = doc.get("m_type").asText()
            var o: Any
            if(t == "Word"){
                o = Util.om.treeToValue<Word>(doc)
            }
            else if(t == "Phrase"){
                o = Util.om.treeToValue<Phrase>(doc)
            }
            else{
                o = object {}
            }

            concepts.add(o)

        }

        return concepts
    }
    suspend fun word_examples(words: List<Word>): List<List<Example>>{
        var examples = mutableListOf<MutableList<Example>>()
        var map = mutableMapOf<String, Int>()
        var e_ids = Util.om.createArrayNode()
        var idx = 0
        for(m in words){
            m.m_examples?.forEach {
                e_ids.add(it)
                map.put(it, idx)
            }
            examples.add(mutableListOf())
            idx++
        }
        var res = _conceptdb.all_docs(e_ids, include_docs = true)
        var rows = res.get("rows")
        for(r in rows){
            val e = Util.om.treeToValue<Example>(r.get("doc"))
            var i = map.get(e.id)?: -1
            var list = examples.get(i)
            list.add(e)
        }
        return examples
    }
    suspend fun word_consonance(words: List<Word>): List<List<String>>{
        val set = mutableSetOf<String>()
        val keys = Util.om.createArrayNode()
        for(w in words){
            if(!set.contains(w.spelling)){
                val key = Util.om.createArrayNode().add(w.spelling)
                keys.add(key)
            }
        }
        val data = _conceptdb.view("app", "consonance")
                .arguments(include_docs = true, keys = keys)
                .retrieve()
        val rows = data.get("results").get(0).get("rows")
        val consonance_map = mutableMapOf<String, List<String>>()
        for(r in rows){
            val doc = r.get("doc") as ObjectNode
            val spelling = doc.get("word").asText()
            val consonance = Util.om.treeToValue<List<String>>(doc.get("consonance"))
            consonance_map.put(spelling, consonance)
        }
        val res = mutableListOf<List<String>>()
        for (w in words){
            val v = consonance_map.get(w.spelling)
            if(v != null){
                res.add(v)
            }
            else{
                res.add(listOf())
            }

        }

        return res
    }
    suspend fun phrase_examples(phrases: List<Phrase>): List<List<Example>>{
        var examples = mutableListOf<MutableList<Example>>()
        var map = mutableMapOf<String, Int>()
        var e_ids = Util.om.createArrayNode()
        var idx = 0
        for(m in phrases){

            m.m_examples?.forEach {
                e_ids.add(it)
                map.put(it, idx)
            }
            examples.add(mutableListOf())
            idx++
        }
        var res = _conceptdb.all_docs(e_ids, include_docs = true)
        var rows = res.get("rows")
        for(r in rows){
            val e = Util.om.treeToValue<Example>(r.get("doc"))
            var i = map.get(e.id)?: -1
            var list = examples.get(i)
            list.add(e)
        }
        return examples
    }

    suspend fun achievement(words: List<Word>, m_student: String): List<Achievement?>{
        val keys = Util.om.createArrayNode()
        for(c in words){
            keys.add(Util.om.createArrayNode().add(m_student).add(c.id))
        }
        val data = _traindb.view("app", "concepts")
                .arguments(include_docs = true, keys = keys)
                .retrieve()

        val rows = data.get("results").get(0).get("rows")
        val achievement_map = mutableMapOf<String, Achievement>()
        for(r in rows){
            val doc = r.get("doc")
            achievement_map.put(doc.get("m_concept").asText(), Util.om.treeToValue(doc))
        }
        val res = mutableListOf<Achievement?>()
        for(c in words){
            res.add(achievement_map.get(c.id))
        }

        return res
    }
    suspend fun ru_tr(words: List<Word>): List<String>{
        val res = mutableListOf<String>()
        for(w in words){
            if(w.tr.isNullOrEmpty()){
                res.add("")
            }
            else{
                val ru_tr = ru_transcrtiption(w.tr?:"")
                res.add(ru_tr)
            }
        }
        return res
    }

    val _sound_map = mapOf(
            "i" to "и",
            "ɪ" to "и",
            "e" to "э",
            "æ" to "э",
            "ɑː" to "аа",
            "ɔ" to "о",
            "ɒ" to "о",
            "ɜ" to "ё",
            "ə" to "а",
            "ʌ" to "а",
            "ʊ" to "у",
            "u" to "у",
            "aɪ" to "ай",
            "ai" to "ай",
            "eɪ" to "ей",
            "ei" to "ей",
            "ɔɪ" to "ой",
            "ɔi" to "ой",
            "aʊ" to "ау",
            "au" to "ау",
            "əʊ" to "оу",
            "ɪə" to "иэ",
            "iə" to "иэ",
            "ʊə" to "уэ",
            "uə" to "уэ",
            "ɛə" to "эа",
            "eə" to "эа",
            "p" to "п",
            "t" to "т",
            "b" to "б",
            "d" to "д",
            "m" to "м",
            "n" to "н",
            "k" to "к",
            "l" to "л",
            "g" to "г",
            "f" to "ф",
            "v" to "в",
            "s" to "с",
            "z" to "з",
            "ʃ" to "ш",
            "ʒ" to "ж",
            "ʧ" to "ч",
            "ʤ" to "дж",
            "r" to "р",
            "h" to "х",
            "j" to "й",
            "ju" to "ю",
            "je" to "е",
            "jɔ" to "ё",
            "jʌ" to "я",
            "w" to "в",
            "ŋ" to "н",
            "θ" to "с",
            "ð" to "з"
    )
    private fun ru_transcrtiption(transcription: String): String{
        val tr = transcription.replace("ˈ", "")

        val lexemes = mutableListOf<String>()
        var idx = 0
        var cur = tr[idx]
        var next = tr[idx]
        while(idx < tr.length - 1){
            cur = tr[idx]
            next = tr[idx + 1]
            if(cur == 'ː'){
                lexemes.add(lexemes[lexemes.size - 1])
                idx++
            }
            else if(cur == ' ' || cur == '-'){
                lexemes.add(" ")
                idx++
            }
            else{
                var v = _sound_map["$cur$next"]
                if(v == null){
                    v = _sound_map[cur.toString()]
                    if(v == null){
                        throw Exception(transcription);
                    }
                    idx++
                }
                else{
                    idx+=2
                }
                lexemes.add(v)
            }
        }
        if(idx < tr.length){
            if(next == 'ː' || cur == ' ' || cur == '-'){
                //lexemes.add(lexemes[lexemes.size - 1])
                idx++
            }
            else{
                val v = _sound_map[next.toString()]
                if(v == null){
                    throw Exception(tr);
                }
                lexemes.add(v)
                idx++
            }
        }
        return lexemes.joinToString("")
    }

    suspend fun concept_save(segments: List<Segment>):Int{
        var lexdb_segments = mutableListOf<ObjectNode>()
        for(i in segments){
            if(_conceptdb_types.contains(i.type)){
                i.unit.put("m_type", i.type)
                if(i.unit.has("id")){
                    i.unit.put("_id", i.unit.get("id").asText())
                    i.unit.remove("id")
                }
                lexdb_segments.add(i.unit)
            }
        }

        if(lexdb_segments.size > 0)
            _conceptdb.save_objs(lexdb_segments)

        return segments.size
    }
    suspend fun consonance_save(word: String, consonance: List<String>): Int{
        // получить объект по слову и сформировать список уникальных
        val data = _conceptdb.view("app", "consonance")
                .arguments(key = Util.om.createArrayNode().add(word), include_docs = true)
                .retrieve()
        var doc = data.get("results").get(0).get("rows").get(0)?.get("doc")
        if(doc == null){
            doc = Util.om.createObjectNode().put("word", word).put("m_type", "Consonance")
        }
        val c = Util.om.valueToTree<ArrayNode>(consonance)
        (doc as ObjectNode).set("consonance", c)

        val docs = Util.om.createArrayNode().add(doc)
        _conceptdb.put(docs)
        return 1
    }
    suspend fun test_save(m_student: String, practices: List<Practice>): Int{
        // получить из БД по студенту и
        var docs = Util.om.createArrayNode()
        val keys = Util.om.createArrayNode()
        val result_map = mutableMapOf<String, Practice>()
        for (p in practices){
            val key = Util.om.createArrayNode()
            key.add(m_student)
            key.add(p.m_concept)
            result_map.put("$m_student-${p.m_concept}", p)
            keys.add(key)
        }
        val data = _traindb.view("app", "concepts")
                .arguments(include_docs = true, keys = keys)
                .retrieve()

        val rows = data.get("results").get(0).get("rows")
        for (r in rows){
            val key= r.get("key")
            val doc = r.get("doc") as ObjectNode
            val k = "${key.get(0).asText()}-${key.get(1).asText()}"
            val r = result_map.remove(k)?.result?: 0

            doc.put("init_progress", r)

            docs.add(doc)
        }

        val id = _traindb.ids(result_map.size)
        var idx = 0
        for(r in result_map){
            val a = Achievement()
            a.m_student = m_student
            a.m_concept = r.value.m_concept
            a.id = id[idx]
            a.image = ""
            a.repetitions = listOf()
            a.init_progress = r.value.result

            val doc = Util.om.valueToTree<ObjectNode>(a)
            //todo переименовать поле id

            doc.put("m_type", "Achievement")
            docs.add(doc)

            idx++
        }

        _traindb.put(docs)

        return  1
    }
    suspend fun repetition_save(m_student: String, practices: List<Practice>): Int{

        var docs = Util.om.createArrayNode()
        val keys = Util.om.createArrayNode()
        val result_map = mutableMapOf<String, Practice>()
        for (p in practices){
            val key = Util.om.createArrayNode()
            key.add(m_student)
            key.add(p.m_concept)
            result_map.put("$m_student-${p.m_concept}", p)
            keys.add(key)
        }
        val data = _traindb.view("app", "concepts")
                .arguments(include_docs = true, keys = keys)
                .retrieve()

        val rows = data.get("results").get(0).get("rows")
        for (r in rows){
            val key= r.get("key")
            val doc = r.get("doc") as ObjectNode
            val k = "${key.get(0).asText()}-${key.get(1).asText()}"
            val r = result_map.remove(k)?.result?: 0

            val reps = doc.get("repetitions") as ArrayNode

            val rep = Util.om.createObjectNode()
            rep.put("datetime", Util.UTC_now().frt())
            rep.put("result", r)

            reps.add(rep)

            docs.add(doc)
        }

        _traindb.put(docs)
        return 1
    }
    suspend fun to_work(m_student: String, m_concept: String, image: String, include: Boolean): Int{
        val key = Util.om.createArrayNode().add(m_student).add(m_concept)
        val data = runBlocking {
            _traindb.view("app", "concepts")
                    .arguments(key = key, include_docs = true)
                    .retrieve()
        }

        val rs = data.get("results").get(0).get("rows")
        if(rs.size() > 0){
            val doc = rs.get(0).get("doc") as ObjectNode
            doc.put("in_process", include)
            doc.put("image", image)
            _traindb.put(Util.om.createArrayNode().add(doc))
        }
        return 1
    }
    fun report(m_student: String): ObjectNode{
        // загрузить данные пользователя с сумарными тренировками
        // для каждой пачки подсчитать значение с 0 средние и больше 5

        // получить все id концептов пользователя с оценкой по реппетициям

        //807803ae594d84510697fd1d2b235edd

        val sk = Util.om.createArrayNode().add(m_student)
        val ek = Util.om.createArrayNode().add(m_student)
        ek.addObject()

        val data1 = runBlocking {
            _traindb.view("app", "concepts")
                    .arguments(startkey = sk, endkey = ek)
                    .retrieve()
        }

        val rows = data1.get("results").get(0).get("rows")

        val keys = Util.om.createArrayNode()
        val score_map = mutableMapOf<String, Pair<Int, Boolean>>()
        for(r in rows){
            val id = r.get("key").get(1).asText()
            val score = r.get("value").get("score").asInt()
            val in_process = r.get("value").get("in_process").asBoolean()

            keys.add(r.get("key").get(1))
            score_map.put(id, Pair(score, in_process))
        }

        val data = runBlocking { _conceptdb.all_docs(include_docs = true, keys = keys) }

        val rs = data.get("rows")
        val report = _base_report.deepCopy()
        for(r in rs){
            val doc = r.get("doc")
            val st = score_map.get(doc.get("_id").asText())
            var category = "udf"
            if(st != null){
                if(st.second){
                    category = "doing"
                }
                else if(st.first >=  10){
                    category = "done"
                }
                else if(st.first > 0){
                    category = "todo"
                }
            }

            val level = doc.get("level").asText()
            val type = doc.get("m_type").asText()
            val topic = doc.get("topic").asText()

            val scores = report.get(level).get(type).get(topic)

            var c = scores.get(category) as ObjectNode

            var weight = c.get("w").asInt() + doc.get("weight").asInt()
            var count = c.get("c").asInt()

            c.put("w", weight)
            c.put("c", count + 1)

            var u = scores.get("udf") as ObjectNode
            u.put("w", u.get("w").asInt() - weight)
            u.put("c", u.get("c").asInt() - 1)
        }

        return report
    }
    suspend fun plan(m_student: String): ObjectNode{
        val sk = Util.om.createArrayNode().add(m_student)
        val ek = Util.om.createArrayNode().add(m_student)
        ek.addObject()
        val data = _traindb.view("app", "plan")
                .arguments(startkey = sk, endkey = ek)
                .retrieve()


        val plan = mutableMapOf<String, Int>()

        val rs = data.get("results").get(0).get("rows")
        val now = Util.UTC_now()
        val tomorrow = Util.UTC_tomorrow()
        for(r in rs){
            val datetime = r.get("key").get(1).asText()

            val current = Util.UTC_parse(datetime)
            var k = current.clear_time().frt()
            if (current < now) {
                k = "now"
            } else if (current < tomorrow) {
                k = "today"
            }
            var count = plan.get(k) ?: 0
            count++
            plan.put(k, count)
        }

        val data2 = _traindb.view("app", "wrongs")
                .arguments(startkey = sk, endkey = ek)
                .retrieve()

        val rows = data2.get("results").get(0).get("rows")
        plan.put("wrongs", rows.size())

        return Util.om.valueToTree(plan)
    }
    suspend fun bundle(m_student: String, type: Bundle_type, topic: String): List<Any>{
        var view = "plan"
        val sk = Util.om.createArrayNode()
        val ek = Util.om.createArrayNode()
        if(type== Bundle_type.now){
            sk.add(m_student)
            ek.add(m_student).add(Util.UTC_now().frt())
        }
        if(type == Bundle_type.today){
            sk.add(m_student)
            ek.add(m_student).add(Util.UTC_tomorrow().frt())
        }
        else if(type == Bundle_type.wrongs){
            // получить топики с ошибками
            view = "wrongs"
            sk.add(m_student)
            ek.add(m_student)
            ek.addObject()
        }
        val concept_data = _traindb.view("app", view)
                .arguments(startkey = sk, endkey = ek , reduce = false)
                .retrieve()
        val concept_rows = concept_data.get("results").get(0).get("rows")
        val ids = Util.om.createArrayNode()
        for(r in concept_rows){
            ids.add(r.get("value").asText())
        }
        val data = _conceptdb.all_docs(ids, include_docs = true)
        var concepts = mutableListOf<Any>()

        val rows = data.get("rows")
        for(r in rows){
            val doc = r.get("doc")
            val t = doc.get("m_type").asText()
            var o: Any
            if(t == "Word"){
                o = Util.om.treeToValue<Word>(doc)
            }
            else if(t == "Phrase"){
                o = Util.om.treeToValue<Phrase>(doc)
            }
            else{
                o = object {}
            }

            concepts.add(o)
        }

        return concepts
    }
}

class Querty_tests {
    @Test
    fun t1() {
        val p = ConceptPvd("", "")

        for (i in 0..2) {
            runBlocking {  p.concepts(listOf("Word", "Phrase"), listOf("A1"), listOf(), listOf("animal"), Range(0F, 1000F), "")}
        }

        val timer = Stopwatch.createUnstarted()
        timer.start()
        val res = runBlocking { p.concepts(listOf("Word", "Phrase"), listOf("A1", "A2"), listOf("verb"), listOf(""), Range(0F, 1000F), "")}
        timer.stop()
        println(timer)
        println(res)
    }
    @Test
    fun t2() {
        val c = couchdb("concepts", "http://localhost:5984")

        val keys = Util.om.createArrayNode()
        keys.add("Phrase_A1")
        keys.add("Phrase_A2")
        keys.add("Word_A1")
        keys.add("Word_A1")

        for (i in 0..2) {
            runBlocking {
                c.list("app", "filter", "words", include_docs = true, keys = keys, group = false)
            }
        }

        val timer = Stopwatch.createUnstarted()
        timer.start()
        val res = runBlocking {
            c.list("app", "filter", "words", include_docs = true, keys = keys, group = false, reduce = false)
        }
        println(timer)
        println(res)
    }
    @Test
    fun test_save() {
        val c = ConceptPvd("http://localhost", "5984")
        val p1 = Practice()
        p1.m_concept = "c123"
        p1.result = 10
        runBlocking {  c.test_save("stud123", listOf(p1)) }
    }
    @Test
    fun achievement() {
        val c = ConceptPvd("http://localhost", "5984")
        val w1 = Word()
        w1.id = "c123"
        val w2 = Word()
        w2.id = "c1234"
        val res = runBlocking {  c.achievement(listOf(w1, w2), "stud123") }
        print(res)
    }
    @Test
    fun report() {
        val c = ConceptPvd("http://localhost", "5984")
        val res = runBlocking {  c.report("stud123") }
        print(res)
        val res2 = runBlocking {  c.report("stud123") }
        print(res2)
        assertEquals(res, res2)
    }
    @Test
    fun repetition_save(){
        val c = ConceptPvd("http://localhost", "5984")
        val p1 = Practice()
        p1.m_concept = "b32129a4062b8b28b84201e86f4adf2f"
        p1.result = 3
        runBlocking {  c.repetition_save("stud123", listOf(p1)) }
    }
    @Test
    fun to_work(){
        val c = ConceptPvd("http://localhost", "5984")
        val r = runBlocking {  c.to_work("stud123", "b32129a4062b8b28b84201e86f10f087", "слива прыгает", true) }

        print(r)
    }
    @Test
    fun plan(){
        val c = ConceptPvd("http://localhost", "5984")
        val r = runBlocking {  c.plan("807803ae594d84510697fd1d2b004875") }

        print(r)
    }
    @Test
    fun bundle_today(){
        val c = ConceptPvd("http://localhost", "5984")
        val r = runBlocking {  c.bundle("807803ae594d84510697fd1d2b004875", Bundle_type.today, "animals") }
        print(r)
    }
    @Test
    fun bundle_wrongs(){
        val c = ConceptPvd("http://localhost", "5984")
        val r = runBlocking {  c.bundle("807803ae594d84510697fd1d2b004875", Bundle_type.wrongs, "animals") }
        print(r)
    }
}