package demo.provider

import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.treeToValue
import com.google.common.base.Stopwatch
import demo.api.couchdb.couchdb
import whynot.entity.*
import demo.util.Util
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


class Email_pvd(val host: String){
    suspend fun confirm_email_send(email: String, code: String){
        val await = async {
            val props = Properties()
            props["mail.smtp.auth"] = "true"
            props["mail.smtp.starttls.enable"] = "true"
            props["mail.smtp.host"] = "smtp.gmail.com"
            props["mail.smtp.port"] = "587"

            val session = Session.getInstance(props, Smtp_authenticator())

            try {
                val message = MimeMessage(session)
                message.setFrom(InternetAddress("whynot.space.drive@gmail.com"))
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email))
                message.subject = "Подтверждение почты"
                message.setText("""Для подтверждения почты нажмите на <a href="${host}/auth/email/confirm?code=${code}">подтвердить</a>""", "utf-8", "html")
                Transport.send(message)
            } catch (e: MessagingException) {
                throw RuntimeException(e)
            }
        }
        await.await()
    }
}


class Smtp_authenticator : Authenticator() {
    override fun getPasswordAuthentication(): PasswordAuthentication {
        return PasswordAuthentication("whynot.space.drive@gmail.com", "qwertywer2709");
    }
}

class Email_pvd_test{
    val pvd = Email_pvd("http://localhost")

    @Test
    fun send_confirm_email(){
        runBlocking { pvd.confirm_email_send("novikov.eugeniy@gmail.com", "123") }
    }
}