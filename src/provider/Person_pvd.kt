package demo.provider

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.treeToValue
import com.google.common.base.Stopwatch
import demo.api.couchdb.couchdb
import whynot.entity.*
import demo.util.Util
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test

class Person_pvd: couchdb{
    constructor(host: String, port: String)
            : super("user", "${host}:${port}")
    suspend fun wn_accounts(ids: List<String> = listOf(), person_ids: List<String> = listOf()): List<WN_account>{
        val keys = Util.om.createArrayNode()
        ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("WN_account")) }
        person_ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("Person").add("WN_account")) }

        val data = view("app", "rel")
                .arguments(reduce = false, include_docs = true, keys = keys)
                .retrieve()
        val rows = data
                .get("results")
                ?.firstOrNull()
                ?.get("rows")
        return rows?.map { Util.om.treeToValue<WN_account>(it.get("doc")) }?: emptyList()
    }
    suspend fun wn_accounts(logins: List<String> = listOf()): List<WN_account>{
        val keys = Util.om.createArrayNode()
        for(i in logins){
            val login = i.trim().toLowerCase()
            if(! login.isNullOrEmpty()){
                keys.add(Util.om.createArrayNode().add("login").add(login))
            }
        }
        if(keys.size() == 0)
            return emptyList()

        val account_data = view("app", "accounts")
                .arguments(reduce = false, include_docs = true, keys = keys)
                .retrieve()
        val rows = account_data
                .get("results")
                ?.firstOrNull()
                ?.get("rows")
        val res = mutableListOf<WN_account>()
        if(rows != null){
            for(r in rows){
                res.add(Util.om.treeToValue(r.get("doc")))
            }
        }
        return res
    }
    suspend fun persons(ids: List<String> = listOf(), account_ids: List<String> = listOf(), contact_ids: List<String> = listOf()): List<Person>{
        val keys = Util.om.createArrayNode()
        ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("WN_account")) }
        account_ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("WN_account").add("Person")) }
        contact_ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("Contact").add("Person")) }

        val data = view("app", "rel")
                .arguments(reduce = false, include_docs = true, keys = keys)
                .retrieve()
        val rows = data
                .get("results")
                ?.firstOrNull()
                ?.get("rows")
        return rows?.map { Util.om.treeToValue<Person>(it.get("doc")) }?: emptyList()
    }
    suspend fun wn_settings(ids: List<String> = listOf(), person_ids: List<String> = listOf()): List<WN_setting>{
        val keys = Util.om.createArrayNode()
        ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("WN_setting")) }
        person_ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("Person").add("WN_setting")) }

        val data = view("app", "rel")
                .arguments(reduce = false, include_docs = true, keys = keys)
                .retrieve()
        val rows = data
                .get("results")
                ?.firstOrNull()
                ?.get("rows")
        return rows?.map { Util.om.treeToValue<WN_setting>(it.get("doc")) }?: emptyList()
    }
    suspend fun contacts(ids: List<String> = listOf(), person_ids: List<String> = listOf()): List<Contact>{
        val keys = Util.om.createArrayNode()
        ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("Contact")) }
        person_ids.forEach { keys.add(Util.om.createArrayNode().add(it).add("Person").add("Contact")) }

        val data = view("app", "rel")
                .arguments(reduce = false, include_docs = true, keys = keys)
                .retrieve()
        val rows = data
                .get("results")
                ?.firstOrNull()
                ?.get("rows")
        return rows?.map { Util.om.treeToValue<Contact>(it.get("doc")) }?: emptyList()
    }
    suspend fun contact(type: Contact_type, value: String):Contact?{
        val key = Util.om.createArrayNode().add(type.toString()).add(value)
        val contact_data = view("app", "contacts")
                .arguments(key = key, include_docs = true)
                .retrieve()
        val contact_json = contact_data.get("results")
                ?.firstOrNull()
                ?.get("rows")
                ?.firstOrNull()
                ?.get("doc") ?: null
        if(contact_json != null){
            return Util.om.treeToValue(contact_json)
        }
        return null
    }
    suspend fun contact(confirm_value: String): Contact?{
        val key = Util.om.createArrayNode().add(confirm_value)
        val contact_data = view("app", "confirm")
                .arguments(key = key, include_docs = true)
                .retrieve()
        val contact_json = contact_data.get("results")
                ?.firstOrNull()
                ?.get("rows")
                ?.firstOrNull()
                ?.get("doc") ?: null
        if(contact_json != null){
            return Util.om.treeToValue(contact_json)
        }
        return null
    }
    suspend fun change_password(account_id: String, old_password: String, new_password: String):Int{
        val a = wn_accounts(ids = listOf(account_id)).firstOrNull()
        if(a != null ){
            val old_hash = Util.sha_256(old_password)
            if(a.password == old_hash){
                val new_hash = Util.sha_256(new_password)
                a.password = new_hash
                save(listOf(a))
            }
            else{
                throw Exception("Current password is not correct")
            }
        }
        return 1
    }
    suspend fun change_email(account_id: String, email: String): Int{
/*
Если  пользователь меняет email
    Если новый email содержится в БД
        Если он принадлежит текущему пользователю
            обновить значение контакта
        Иначе
            вернуть ошибку
    Иначе
        Добавить новый контакт
        Отправить email для подтверждения
*/
        val e = email.trim().toLowerCase()
        val a = wn_accounts(ids = listOf(account_id)).firstOrNull()
        if(a != null){
            val c = contact(Contact_type.email, e)
            if(c != null && c.m_person != a.m_person){
                throw Exception("Person is not owner of email")
            }

            val nc = contact(Contact_type.email, a.login)

            a.login = e
            val upds = mutableListOf<Any>(a)
            if(nc != null){
                nc.value = e
                nc.confirmed = false
                upds.add(nc)
            }
            save(upds)
        }
        return 1
    }
    suspend fun change_purpose(account_id: String, purpose: String): Int{
        val a = wn_accounts(ids = listOf(account_id)).firstOrNull()
        if(a != null){
            var s = wn_settings(person_ids = listOf(a.m_person)).firstOrNull()
            if(s == null){
                s = WN_setting()
                s.m_person = a.m_person
            }
            s?.purpose = purpose
            save(listOf(s))
        }
        return 1
    }
}

class UserPvdTest{
    val host = "http://109.196.164.117"
    //val host = "http://localhost"
    val pvd = Person_pvd(host, "5984")

    @Test
    fun wn_account(){
        val res = runBlocking {  pvd.wn_accounts(person_ids = listOf("2be236cc70b9fb75e2aa7e3d2a5dbb15"))}
        print(res.first().login)
    }
    @Test
    fun save(){
        runBlocking {
            val timer = Stopwatch.createUnstarted()
            timer.start()

            val p = Person()
            p.id = pvd.get_id()
            p.firstname = "Евгений"
            p.lastname = "Новиков"
            p.middlename = "Олегович"

            val c = Contact()
            c.id = pvd.get_id()
            c.type = Contact_type.email
            c.value = "novikov.eugeniy@gmail.com"
            c.m_person = p.id
            pvd.save(listOf(p, c))

            timer.stop()
            println(timer)
           ""
        }
    }


}