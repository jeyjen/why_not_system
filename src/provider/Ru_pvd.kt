package demo.provider

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement

class Ru_pvd{
    var _con : Connection? = null
    var stmt : PreparedStatement? = null

    init{
        Class.forName("org.sqlite.JDBC")
        _con = DriverManager.getConnection("jdbc:sqlite:lexis.db")
        val q = """
select
    word
from
    words
where
    word like ?
    and type != 'глаг'
    and type != 'прил'
order by word
    """
        stmt = _con?.prepareStatement(q)
    }

    fun consonance(transcription: String):List<String>{
        val ru_tr = ru_transcrtiption(transcription)
        val set = mutableSetOf<String>()
        val result = mutableListOf<String>()
        val max = if(ru_tr.length > 4)4 else ru_tr.length
        loop@for(i in max downTo 2){
            val prefix = ru_tr.substring(0, i)
            val vs = mutableListOf(prefix)
            if(prefix.contains("э"))
                vs.add(prefix.replace('э', 'е'))
            if(prefix.contains("ё"))
                vs.add(prefix.replace('ё', 'е'))

            for (v in vs){
                var res = runBlocking { get_with_prefix(v) }
                for(r in res){

                    if(set.contains(r)|| r.contains('-'))
                        continue
                    set.add(r)
                    result.add(r)
                    if(result.size >= 500)
                        break@loop
                }
            }
        }

        return result
    }
    fun ru_by_prefix(prefixes: List<String>):List<String>{
        val set = mutableSetOf<String>()
        val result = mutableListOf<String>()
        for(prefix in prefixes){
            var res = runBlocking { get_with_prefix(prefix.toLowerCase()) }
            for(r in res){
                if(set.contains(r)|| r.contains('-'))
                    continue
                set.add(r)
                result.add(r)
            }
        }
        return result
    }
    private suspend fun get_with_prefix(part: String): List<String>{
        val res = async {
            val coincidences = mutableListOf<String>()

            stmt?.setString(1, "${part}%")
            val set = stmt?.executeQuery()
            set?.let {
                while (set.next()){
                    val w = set.getString("word")
                    coincidences.add(w)
                }
                set.close()
            }
            coincidences
        }
        return res.await()
    }
    private fun ru_transcrtiption(transcription: String): String{
        //tier [ˈtaɪə] сущ

        val map = mapOf(
                "i" to "и",
                "ɪ" to "и",
                "e" to "э",
                "æ" to "э",
                "ɑː" to "аа",
                "ɔ" to "о",
                "ɒ" to "о",
                "ɜ" to "ё",
                "ə" to "а",
                "ʌ" to "а",
                "ʊ" to "у",
                "u" to "у",
                "aɪ" to "ай",
                "ai" to "ай",
                "eɪ" to "ей",
                "ei" to "ей",
                "ɔɪ" to "ой",
                "ɔi" to "ой",
                "aʊ" to "ау",
                "au" to "ау",
                "əʊ" to "оу",
                "ɪə" to "иэ",
                "iə" to "иэ",
                "ʊə" to "уэ",
                "uə" to "уэ",
                "ɛə" to "эа",
                "eə" to "эа",
                "p" to "п",
                "t" to "т",
                "b" to "б",
                "d" to "д",
                "m" to "м",
                "n" to "н",
                "k" to "к",
                "l" to "л",
                "g" to "г",
                "f" to "ф",
                "v" to "в",
                "s" to "с",
                "z" to "з",
                "ʃ" to "ш",
                "ʒ" to "ж",
                "ʧ" to "ч",
                "ʤ" to "дж",
                "r" to "р",
                "h" to "х",
                "j" to "й",
                "ju" to "ю",
                "je" to "е",
                "jɔ" to "ё",
                "jʌ" to "я",
                "w" to "в",
                "ŋ" to "н",
                "θ" to "с",
                "ð" to "з"
        )


        // выделены лексемы

        val tr = transcription.replace("ˈ", "")

        val lexemes = mutableListOf<String>()
        var idx = 0
        var cur = '1'
        var next = '1'
        while(idx < tr.length - 1){
            cur = tr[idx]
            next = tr[idx + 1]
            if(cur == 'ː'){
                //lexemes.add(lexemes[lexemes.size - 1])
                idx++
            }
            else{
                var v = map["$cur$next"]
                if(v == null){
                    v = map[cur.toString()]
                    if(v == null){
                        throw Exception(transcription);
                    }
                    idx++
                }
                else{
                    idx+=2
                }
                lexemes.add(v)
            }
        }
        if(idx < tr.length){
            if(next == 'ː'){
                //lexemes.add(lexemes[lexemes.size - 1])
                idx++
            }
            else{
                val v = map[next.toString()]
                if(v == null){
                    throw Exception(tr);
                }
                lexemes.add(v)
                idx++
            }
        }
        return lexemes.joinToString("")
    }
}