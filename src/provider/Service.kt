package demo.provider

import com.fasterxml.jackson.databind.JsonNode
import demo.util.Util
import io.ktor.client.request.get
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import java.net.URLEncoder
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement

class Service{
    val _poses_map = mapOf(
            "vrb" to "verb",
            "nn" to "noun",
            "adv" to "adverb",
            "adj" to "adjective",
            "num" to "number",
            "ptp" to "ptp",
            "prn" to "pronoun",
            "prp" to "preposition",
            "cnj" to "conjunction",
            "pt" to "particle",
            "frn" to "frn",
            "inv" to "inv",
            "pdv" to "pdv",
            "pth" to "parenthesis",
            "inj" to "interjection",
            "dee" to "participle"
    )

    suspend fun translate(lexeme: String, pos: String): List<Translate>{
        val result = mutableListOf<Translate>()
        val url = "https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=ru&srv=tr-text&sid=e3ad9896.5b31fa49.2f9fd672&dict=en-ru.regular&flags=231&text=${URLEncoder.encode(lexeme, "UTF-8")}"
        var res = Util.http.get<JsonNode>(url)
        var reg = res.get("en-ru")?.get("regular")
        if(reg == null){
            res.get("en-ru")?.get("def")
        }

        reg?.let {
            for(r in reg){
                val pos_code = r.get("pos")?.get("code")?.asText()?:""
                var p = _poses_map.get(pos_code)?:""
                // преобразовать pos code
                if(p=="particle" && pos == "exclamation"){
                    p = "exclamation"
                }
//                if(! pos.isNullOrEmpty() && ! pos.equals(p))
//                    continue

                val tr = r.get("tr")
                for(t in tr){
                    var translate = t.get("text").asText()
                    val mean = t.get("mean")?.map { it.get("text").asText() }?.joinToString(" | ") ?: ""
                    result.add(Translate(translate, p, mean))
                }
            }
        }

        return result
    }
}

data class Translate(
        var translate: String = "",
        var pos: String = "",
        var mean: String = ""
)