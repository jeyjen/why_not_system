package demo.provider

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import demo.util.JacksonSerializer
import io.ktor.client.HttpClient
import io.ktor.client.engine.config
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.header
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import kotlinx.coroutines.experimental.delay
import org.apache.http.HttpHost
import org.jsoup.Jsoup
import java.io.File


class en_profile{
    val _host = "http://vocabulary.englishprofile.org"
    private val _client = HttpClient(io.ktor.client.engine.apache.Apache.config {
        customizeClient {
            // Apache's HttpAsyncClientBuilder
            setProxy(HttpHost("127.0.0.1", 8888))
            //setMaxConnTotal(1000) // Maximum number of socket connections.
            //setMaxConnPerRoute(100) // Maximum number of requests for a specific endpoint route.
        }
    }) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }

    private val _topics = get_topics()
    private val _idioms = mutableSetOf<String>()


    //private val _keys = mutableMapOf<String, JsonNode>()
    private val _words = mutableMapOf<String, JsonNode>()
    private val _phrases = mutableMapOf<String, JsonNode>()
    private val _phrase_verbs = mutableMapOf<String, JsonNode>()

    suspend fun retrieve(){
        val levels = listOf(301, 302, 303, 304, 305, 306)
        val part_of_speach = listOf(10, 11, 12, 13, 14, 15, 19, 20, 23, 25)
        val om = ObjectMapper()

        // определены идиомы
        val html = request(HttpMethod.Get, "/dictionary/search/uk/?pageSize=10000&wl=&q=&c=4&c=-1&c=-1&c=-1&c=-1&c=-1&c=-1")

        val doc = Jsoup.parse(html)
        val idioms = doc.select("#search-results > div.search-block > ul > li > a > span > span.base")
        for(idiom in idioms){
            _idioms.add(idiom.text())
        }
        println("extracted idioms")

        // определены темы
//        var topic_options = mutableMapOf(
//                78 to "animals",
//                79 to "arts and media",
//                80 to "body and health",
//                82 to "clothes",
//                83 to "communication",
//                84 to "Crime",
//                85 to "describing things",
//                86 to "education",
//                87 to "food and drink",
//                81 to "homes and buildings",
//                88 to "money",
//                89 to "natural world",
//                90 to "people: actions",
//                91 to "people: appearance",
//                92 to "people: personality",
//                99 to "politics",
//                93 to "relationships",
//                94 to "shopping",
//                95 to "sport and games",
//                96 to "technology",
//                97 to "travel",
//                98 to "work"
//        )
//
//        for(option in topic_options){
//            val html = Request(HttpMethod.Get, "/dictionary/search/uk/?pageSize=10000&wl=&q=&c=-1&c=-1&c=-1&c=-1&c=${option.key}&c=-1&c=-1")
//            val doc = Jsoup.parse(html)
//            val els = doc.select("#search-results > div.search-block > ul > li > a > span.arl1")
//            for (el in els){
//                val base = el.getElementsByClass("base").text()
//                val mean = el.getElementsByClass("gw").text().toLowerCase()
//                val key = "$base#$mean"
//                _topics.put(key, option.value)
//            }
//        }
//
//        File("topics.txt").bufferedWriter().use { out ->
//            for (w in _topics){
//                out.write("${w.key};${w.value}\n")
//            }
//        }


        var word_count = 0
        var phrase_count = 0
        var phrase_verb_count = 0
        val urls_parsed = mutableSetOf<String>()
        loop@ for(lvl in levels){
            for(pos in part_of_speach){
                // urls с определениями
                var base_html = request(HttpMethod.Get, "/dictionary/search/uk/?pageSize=10000&wl=${lvl}&q=&c=1&c=${pos}&c=-1&c=-1&c=-1&c=-1&c=-1")
                val urls = extract_urls(base_html)
                //val urls = mutableSetOf("/dictionary/show/uk/1026864")
                // /dictionary/show/uk/1002058 - idiom

                for(url in urls){
                    if(urls_parsed.contains(url))
                        continue

                    val html = request(HttpMethod.Get, url)

                    val word = om.createObjectNode()
                    val family = om.createObjectNode()

                    val word_sences  = om.createArrayNode()

                    val doc = Jsoup.parse(html)
                    var word_spelling = doc.select("div.head>h1.hw")[0].text()
                    word.put("spelling", word_spelling.toLowerCase())
                    word.put("type", "word")
                    word.set("family", family)
                    val irregular_forms = om.createArrayNode()
                    word.set("irregular_forms", irregular_forms)
                    word.set("senses", word_sences)
                    var els = doc.select("#dictionary_entry div.WordBuilder > div.section")
                    for(el in els){
                        val key = el.getElementsByTag("b").text().toLowerCase()
                        val values = el.getElementsByTag("span").text().split(",").map { it.trim() }
                        family.set(key, om.valueToTree(values))
                    }
                    _words.put(word_spelling, word)

                    // получены значения слова
                    val posblocks = doc.select("div#dictionary_entry div.posblock")


                    for (posblock in posblocks){

                        // выбраны слова
                        val posgramms = posblock.select(">span.posgram")
                        val js_poses = om.createArrayNode()
                        val common_grams = mutableListOf<String>()

                        for(posgramm in posgramms){
                            val pos = posgramm.getElementsByClass("pos")
                            js_poses.add(pos.text())

                            var grams = posgramm.select("span.grams>abbr.gram")
                            for(gram in grams){
                                val gr = gram.text()
                                common_grams.add(gr)
                            }

                            val forms = posblock.select("span.infgrp>b.infl>span.inf")
                            for(form in forms){
                                irregular_forms.add(form.text())
                            }
                        }



                        val gwblocks_sences = posblock.select(">div.gwblock")
                                .filter{ el -> el.select(">div.sense").count() > 0}

                        for (gwblock in gwblocks_sences){
                            val js_sence = om.createObjectNode()

                            val mean = gwblock.getElementsByClass("gw").text().toLowerCase()
                            val s = gwblock.getElementsByClass("sense")
                            val level = s.select("span[class*='freq-']").text()
                            val def = s.select("span.def").text()
                            var examples = s.select("div.examp-block>blockquote.examp")
                            val js_examples = om.createArrayNode()
                            for(example in examples){
                                js_examples.add(example.text())
                            }
                            val js_grams = om.createArrayNode()
                            val grams = s.select("span.grams>abbr.gram")
                            for(gram in common_grams){
                                js_grams.add(gram)
                            }
                            for (gram in grams){
                                js_grams.add(gram.text())
                            }

                            val key = "$word_spelling#$mean";
                            js_sence.set("pos", js_poses)
                            js_sence.put("level", level)
                            js_sence.put("mean", mean)
                            js_sence.put("def", def)
                            js_sence.set("grams", js_grams)
                            js_sence.set("word_examples", js_examples)

                            val topic = _topics[key]
                            if(topic == null){
                                js_sence.set("topics", om.createArrayNode())
                            }
                            else{
                                js_sence.set("topics", om.createArrayNode().add(topic))
                            }

                            word_sences.add(js_sence)

                            //_keys.put(key, js_sence)
                        }

                        // выбраны фразовые глаголы
                        val phrase_verbs = doc.select("div.phrasal_verb")
                        val js_pos = om.createArrayNode().add("phrase verb")
                        for(pv in phrase_verbs){

                            val js_phrase_verb = om.createObjectNode()
                            val js_senses = om.createArrayNode()

                            val phrase_verb = pv.select("span.pv_head>h3.phrase").text()
                            js_phrase_verb.put("spelling", phrase_verb)
                            js_phrase_verb.put("type", "phrase_verb")
                            js_phrase_verb.put("base", word_spelling)
                            js_phrase_verb.set("senses", js_senses)
                            val gwblocks_phrase_verbs = pv.select("div.gwblock:has(div.gwblock > div.sense)")
                            for (gwblock in gwblocks_phrase_verbs){
                                val js_sence = om.createObjectNode()
                                val mean = gwblock.getElementsByClass("gw").text().toLowerCase()
                                val s = gwblock.getElementsByClass("sense")

                                val level = s.select("span[class*='freq-']").text()
                                val def = s.select("span.def").text()
                                var examples = s.select("div.examp-block>blockquote.examp")
                                val js_examples = om.createArrayNode()
                                for(example in examples){
                                    js_examples.add(example.text())
                                }
                                val js_grams = om.createArrayNode()
                                val grams = s.select("span.grams>abbr.gram")
                                for(gram in common_grams){
                                    js_grams.add(gram)
                                }
                                for (gram in grams){
                                    js_grams.add(gram.text())
                                }

                                js_sence.set("pos", js_pos) // TODO реализован собственный способ получения части речи
                                js_sence.put("mean", mean)
                                js_sence.put("level", level)
                                js_sence.put("def", def)
                                js_sence.set("grams", js_grams)
                                js_sence.set("word_examples", js_examples)

                                val key = "$phrase_verb#$mean";
                                val topic = _topics[key]
                                if(topic == null){
                                    js_sence.set("topics", om.createArrayNode())
                                }
                                else{
                                    js_sence.set("topics", om.createArrayNode().add(topic))
                                }

                                js_senses.add(js_sence)
                                //_keys.put(key, js_sence)
                            }

                            // выбраны фразы из словаря
                            val gwblocks_phrases = pv.select("div.gwblock>div.phraserec")
                            for (gwblock in gwblocks_phrases){
                                val js_phrase = om.createObjectNode()

                                val phrase = gwblock.getElementsByClass("phrase").text()
                                js_phrase.put("spelling", phrase)
                                js_phrase.put("type", if(_idioms.contains(phrase)) "idiom" else "phrase")
                                js_phrase.put("base", phrase_verb)

                                val js_sence = om.createObjectNode()
                                js_phrase.set("senses", om.createArrayNode().add(js_sence))

                                val s = gwblock.getElementsByClass("sense")

                                val level = s.select("span[class*='freq-']").text()
                                val def = s.select("span.def").text()
                                var examples = s.select("div.examp-block>blockquote.examp")
                                val js_examples = om.createArrayNode()
                                for(example in examples){
                                    js_examples.add(example.text())
                                }
                                val js_grams = om.createArrayNode()
                                val grams = s.select("span.grams>abbr.gram")
                                for (gram in grams){
                                    js_grams.add(gram.text())
                                }

                                val key = phrase+"#";
                                js_sence.put("mean", "")
                                js_sence.put("level", level)
                                js_sence.put("def", def)
                                js_sence.set("grams", js_grams)
                                js_sence.set("word_examples", js_examples)

                                val topic = _topics[key]
                                if(topic == null){
                                    js_sence.set("topics", om.createArrayNode())
                                }
                                else{
                                    js_sence.set("topics", om.createArrayNode().add(topic))
                                }

                                _phrases.put(phrase, js_phrase)

                                //_keys.put(key, js_sence)
                            }

                            _phrase_verbs.put(phrase_verb, js_phrase_verb)
                        }

                        // выбраны фразы
                        val gwblocks_phrases = doc.select("div.posblock>div.gwblock>div.phraserec")
                        for (gwblock in gwblocks_phrases){
                            val js_phrase = om.createObjectNode()

                            val phrase = gwblock.getElementsByClass("phrase").text()
                            js_phrase.put("spelling", phrase)
                            js_phrase.put("type", if(_idioms.contains(phrase)) "idiom" else "phrase")
                            js_phrase.put("base", word_spelling)

                            val js_sence = om.createObjectNode()
                            js_phrase.set("senses", om.createArrayNode().add(js_sence))

                            val s = gwblock.getElementsByClass("sense")

                            val level = s.select("span[class*='freq-']").text()
                            val def = s.select("span.def").text()
                            var examples = s.select("div.examp-block>blockquote.examp")
                            val js_examples = om.createArrayNode()
                            for(example in examples){
                                js_examples.add(example.text())
                            }
                            val js_grams = om.createArrayNode()
                            val grams = s.select("span.grams>abbr.gram")
                            for (gram in grams){
                                js_grams.add(gram.text())
                            }

                            val key = phrase+"#";
                            js_sence.put("mean", "")
                            js_sence.put("level", level)
                            js_sence.put("def", def)
                            js_sence.set("grams", js_grams)
                            js_sence.set("word_examples", js_examples)

                            val topic = _topics[key]
                            if(topic == null){
                                js_sence.set("topics", om.createArrayNode())
                            }
                            else{
                                js_sence.set("topics", om.createArrayNode().add(topic))
                            }

                            _phrases.put(phrase, js_phrase)
                        }
                    }


                    // реализовано сохранения в файл
                    var f = File("vocabular.txt")

                    for (w in _words){
                        f.appendText("${w.value.toString()}\n")
                    }
                    for (w in _phrase_verbs){
                        f.appendText("${w.value.toString()}\n")
                    }
                    for (w in _phrases){
                        f.appendText("${w.value.toString()}\n")
                    }

//                    File("vocabular.txt").bufferedWriter().use { out ->
//                        for (w in _words){
//                            out.append("${w.value.toString()}\n")
//                        }
//                        for (w in _phrase_verbs){
//                            out.append("${w.value.toString()}\n")
//                        }
//                        for (w in _phrases){
//                            out.append("${w.value.toString()}\n")
//                        }
//                    }

                    urls_parsed.add(url)

                    println("extracted level: ${lvl} pos: ${pos} words: ${_words.count()} phrase: ${_phrases.count()} phrase verb: ${_phrase_verbs.count()}")

                    word_count += _words.count()
                    phrase_count += _phrases.count()
                    phrase_verb_count += _phrase_verbs.count()

                    _words.clear()
                    _phrases.clear()
                    _phrase_verbs.clear()

                    //break@loop
                }
            }
        }

        println()
        println("common: words: ${word_count} phrase: ${phrase_count} phrase verb: ${phrase_verb_count}")
    }


    private fun extract_urls(html: String): Set<String>{
        val urls = mutableSetOf<String>()
        val doc = Jsoup.parse(html)
        val els = doc.select("#search-results > div.search-block > ul > li > a")
        var count = 1
        for(el in els){
            var url = el.attr("href")
            if(url != null){
                url = url.split('?')[0]
                if(!urls.contains(url)){
                    urls.add(url)
                    count ++
                }
            }
        }
        return urls
    }
    private suspend fun upload(verb: HttpMethod, url: String, content: JsonNode): String {
        var res = ""
        var ok = false
        while (!ok) {
            try {
                res = _client.request<String> {
                    url("http://localhost:5984/lex/_bulk_docs")
                    method = verb
                    header("Content-Type", "application/json")
                    header("Accept", "application/json, text/javascript, */*; q=0.01")
                    body = content?:""
                }
                ok = true
            } catch (e: Exception) {
                delay(2000)
            }
        }

        return res
    }
    private suspend fun request(verb: HttpMethod, url: String): String {
        var res = ""
        var ok = false
        while (!ok) {
            try {
                res = _client.request<String> {
                    url("$_host$url")
                    method = verb
                    header("Content-Type", "application/json")
                    header("Authorization", "Basic ZW5nbGlzaHByb2ZpbGU6dm9jYWJ1bGFyeQ==")
                    header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                    //body = content?: ""
                }
                ok = true
            } catch (e: Exception) {
                delay(2000)
            }
        }

        return res
    }


    private fun get_topics() : MutableMap<String, String>{
        val map = mutableMapOf<String, String>()

        File("topics.txt").forEachLine {
            val parts = it.split(';')
            map.put(parts[0], parts[1])
        }

        return map
    }
    // парсинг грамматики
        //http://vocabulary.englishprofile.org/dictionary/show/uk/1005047 - в заголовке
        // http://vocabulary.englishprofile.org/dictionary/show/uk/1001681?showLevel=a1_c2#3001773 - в теле определения

    // парсинг способа использования
        // http://vocabulary.englishprofile.org/dictionary/show/uk/1004923?showLevel=a1_c2#2183757

    // http://vocabulary.englishprofile.org/dictionary/show/uk/1061116?showLevel=a1_c2#3363249 - проверка извлечения грамматических правил для слова
    //  /dictionary/show/uk/1003203

    //= posblock.select("div.gwblock:has(div.gwblock > div.sense)")
}