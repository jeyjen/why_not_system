package demo.provider
import java.sql.DriverManager

class phrase2{
    fun retrieve_morphs(end: String, words: List<String>): Map<String, String>{
        val res = mutableMapOf<String, String>()
        val ws = words.joinToString(", ", "", "", -1, "", {"'${it}'"})
        val s = """
select
    w.lemma, w2.lemma as morph parse_call words w
left join lexlinks ll1
    on ll1.word1id = w.wordid
left join words w2
    on w2.wordid = ll1.word2id
where w.lemma in (%s) and w2.lemma like '%%%s' and ll1.linkid = 81
group by
    w2.lemma
Union
select
    lemma, m.morph parse_call words w
left join morphmaps mm
    on mm.wordid = w.wordid
left join morphs m
    on m.morphid = mm.morphid
where w.lemma in (%s) and m.morph like '%%%s'"""

        val q = s.format(ws, end, ws, end)

        Class.forName("org.sqlite.JDBC")
        val con = DriverManager.getConnection("jdbc:sqlite:D:\\prj\\im\\marketing\\word-net-en.db")

        val stmt = con.createStatement()
        val set = stmt.executeQuery(q)


        while (set.next()){
            val w = set.getString("lemma")
            val v = set.getString("morph")
            if(set.wasNull()){
                res.put(w, "")
            }
            else{
                res.put(w, v)
            }
        }
        set.close()
        stmt.close()
        con.close()

        return res
    }
    fun define_end_s(word: String): String{
        var res = ""
        if(word.equals("quiz")){
            res = "quizzes"
        }
        else if(word.endsWith("x")
                || word.endsWith("s")
                || word.endsWith("ss")
                || word.endsWith("x")
                || word.endsWith("z")
                || word.endsWith("ch")
                || word.endsWith("sh"))
            res = "${word}es"
        else if(word.endsWith("y")){
            if("bcdfghjklmnpqrstvwxz".contains(word[word.length - 2])){
                res = word.substring(0, word.length - 1) + "ies"
            }
            else{
                res = "${word}s"
            }
        }
        else if(word.endsWith("o")){
            if("bcdfghjklmnpqrstvwxz".contains(word[word.length - 2])){
                res = "${word}es"
            }
            else{
                res = "${word}s"
            }
        }
        else
            res = "${word}s"
        return res
    }
    fun define_end(end: String, words: List<String>): List<String>{
        val ws = words.toHashSet()
        val res = mutableListOf<String>()
        when(end){
            "ing"->{
                val ms = retrieve_morphs("ing", words)
                for(p in ms){
                    ws.remove(p.key)
                    res.add(p.value)
                }
                for(w in ws){
                    if(w.equals("have")){
                        res.add("${w.substring(0, w.length - 1)}ing")
                    }
                    else{
                        res.add("${w}ing")
                    }
                }
            }
            "ed"->{
                val ms = retrieve_morphs("ed", words)
                for(p in ms){
                    ws.remove(p.key)
                    res.add(p.value)
                }
                for(w in ws){
                    if(w.endsWith("e")){
                        res.add("${w}d")
                    }
                    else{
                        res.add("${w}ed")
                    }

                }
            }
            "s"->{
                for (w in words){
                    res.add(define_end_s(w))
                }
            }
        }
        return res
    }
    fun generate(pattern: String, variables: Map<String, List<String>>): List<String>{
        val vars = variables.toMutableMap()
        //var ptrn = pattern

        val res = mutableListOf<String>()
        val reg = "@\\w+(:\\w+)*".toRegex()
        val ms = reg.findAll(pattern)

        val max = IntArray(ms.count())
        var idx = IntArray(ms.count())
        // заполнены максимальные индексы
        val keys = mutableListOf<String>()
        var i = 0
        for(m in ms){
            //var key = m.value
            var end = m.value.substringAfter(':', "")
//            val del_idx = m.value.indexOf(':')
//            if(del_idx > 0){
//                //key = m.value.substring(0, del_idx)
//                end = m.value.substring(del_idx + 1, m.value.count())
//            }
            val ws = vars[m.value]!!
            if(! end.isEmpty()){
                vars[m.value] = define_end(end, ws)
                //ptrn = ptrn.replace(m.value, key)
            }
            keys.add(m.value)
            max[i] = ws.count()
            i++
        }

        // для каждой переменной найти общее количество

        do{
            for(l in 0 until max[max.size - 1]){
                idx[idx.size - 1] = l
                var phrase = pattern
                for(i in 0 until idx.size){
                    var j = idx[i]
                    var key = keys[i]
                    var word = vars[key]?.get(j)?: ""

                    phrase = phrase.replace(key, word)
                }

                res.add(phrase)
                //idx[idx.size - 1] + 1
            }
            var t = idx.size - 1
            while(idx[t] >= max[t] - 1 && t > 0){
                idx[t] = 0
                t--
                idx[t] = idx[t] + 1
            }
        }while(less(max, idx))
        // получены все переменные паттерна
        // составлен стек вариантов
        // конкатенирована фраза для каждого варианта подстановочных слов
        return res
    }
    fun less(max: IntArray, idx: IntArray): Boolean{
        for (i in max.size - 2 downTo 0){
            if(idx[i] < max[i])
                return true
        }
        return false
    }
}