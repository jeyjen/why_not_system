package demo

class transcription{
    val _ru_map = mapOf(
            "i" to "и",
            "ɪ" to "и",
            "e" to "э",
            "æ" to "э",
            "ɑː" to "аа",
            "ɔ" to "о",
            "ɒ" to "о",
            "ɜ" to "ё",
            "ə" to "а",
            "ʌ" to "а",
            "ʊ" to "у",
            "u" to "у",
            "aɪ" to "ай",
            "ai" to "ай",
            "eɪ" to "ей",
            "ei" to "ей",
            "ɔɪ" to "ой",
            "ɔi" to "ой",
            "aʊ" to "ау",
            "au" to "ау",
            "əʊ" to "оу",
            "ɪə" to "иэ",
            "iə" to "иэ",
            "ʊə" to "уэ",
            "uə" to "уэ",
            "ɛə" to "эа",
            "eə" to "эа",
            "p" to "п",
            "t" to "т",
            "b" to "б",
            "d" to "д",
            "m" to "м",
            "n" to "н",
            "k" to "к",
            "l" to "л",
            "g" to "г",
            "f" to "ф",
            "v" to "в",
            "s" to "с",
            "z" to "з",
            "ʃ" to "ш",
            "ʒ" to "ж",
            "ʧ" to "ч",
            "ʤ" to "дж",
            "r" to "р",
            "h" to "х",
            "j" to "й",
            "ju" to "ю",
            "je" to "е",
            "jɔ" to "ё",
            "jʌ" to "я",
            "w" to "в",
            "ŋ" to "н",
            "θ" to "с",
            "ð" to "з"
    )

    fun ru_transcribe(transcription: String, with_long : Boolean = true):String {
        val tr = transcription.replace("ˈ", "")
        val lexemes = mutableListOf<String>()
        var idx = 0
        var cur = '1'
        var next = '1'
        while(idx < tr.length - 1){
            cur = tr[idx]
            next = tr[idx + 1]
            if(cur == 'ː'){
                if(with_long){
                    lexemes.add(lexemes[lexemes.size - 1])
                }
                idx++
            }
            else{
                var v = _ru_map["$cur$next"]
                if(v == null){
                    v = _ru_map[cur.toString()]
                    if(v == null){
                        throw Exception(transcription);
                    }
                    idx++
                }
                else{
                    idx += 2
                }
                lexemes.add(v)
            }
            //idx++
        }
        if(idx < tr.length){
            if(next == 'ː'){
                if(with_long){
                    lexemes.add(lexemes[lexemes.size - 1])
                }

                idx++
            }
            else{
                val v = _ru_map[next.toString()]
                if(v == null){
                    throw Exception(tr);
                }
                lexemes.add(v)
                idx++
            }

        }
        return lexemes.joinToString("")
    }

    fun transcribe(transcription: String, lang: String, with_long : Boolean = true): String{
        when(lang){
            "ru"-> return ru_transcribe(transcription, with_long)
            else -> return ""
        }
    }
}