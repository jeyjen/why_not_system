package demo.provider

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.client.HttpClient
import io.ktor.client.call.TypeInfo
import io.ktor.client.engine.apache.Apache
import io.ktor.client.engine.config
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.JsonSerializer
import io.ktor.client.request.get
import io.ktor.client.request.url
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.content.OutgoingContent
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.contentType
import org.apache.http.HttpHost

class yandex{
    var _client = HttpClient(Apache.config {
//        customizeClient {
//            // Apache's HttpAsyncClientBuilder
//            setProxy(HttpHost("127.0.0.1", 8888))
//            setMaxConnTotal(1000) // Maximum number of socket connections.
//            setMaxConnPerRoute(100) // Maximum number of requests for a specific endpoint route.
//        }
    }) {
//        install(JsonFeature) {
//            serializer = JacksonSerializer()
//        }
    }
    suspend fun translate_word(word: String): JsonNode{
        return _client.get<JsonNode>{
            url("https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=ru&srv=tr-text&sid=e3ad9896.5b31fa49.2f9fd672&dict=en-ru.regular&flags=231&text=${word}")
        }
    }
    suspend fun translate_phrase(phrase: String): JsonNode{
        return _client.get<JsonNode>{
            url("https://translate.yandex.net/api/v1/tr.json/translate?id=476873f8.5b3cb91d.7700a1c3-7-0&srv=tr-text&lang=en-ru&reason=paste&text=${phrase}&options=4")
        }
    }

//    dict.1.1.20180626T083955Z.8aa60807ef1e446f.701164c6419f3f5ea2a6b3741ff60a0be3e1b7d2
//    dict.1.1.20171120T143348Z.5c77d82f70db80d4.a45d07f1b270d22f8afb293472a5e82d910b1293


//    val _token = "dict.1.1.20171120T143348Z.5c77d82f70db80d4.a45d07f1b270d22f8afb293472a5e82d910b1293"

//    fun get_langs(): JsonNode? {
//        val r = "https://dictionary.yandex.net/api/v1/dicservice.json/getLangs?key=${_token}".httpGet()
//        val resp = r.responseString()
//        val _om = ObjectMapper()
//        try{
//            return  _om.readTree(resp.third.component1())
//        }
//        catch(e: Exception){}
//        return null
//    }


    //https://dictionary.yandex.net/api/v1/dicservice.json/getLangs?key=API-ключ
    // https://speller.yandex.net/services/spellservice.json/checkText
    // sid=e3ad9896.5b31fa49.2f9fd672&text=heap&lang=en&options=516

    // https://translate.yandex.net/api/v1/tr.json/translate?id=e3ad9896.5b31fa49.2f9fd672-30-0&srv=tr-text&lang=en-ru&reason=enter&options=4&text=heap


    // получение родственных слов
    //https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=ru&srv=tr-text&sid=e3ad9896.5b31fa49.2f9fd672&text=heap&dict=en.syn%2Cen.ant%2Cen.deriv&flags=39

    // получение перевода слов + примеры
    //https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=ru&srv=tr-text&sid=e3ad9896.5b31fa49.2f9fd672&text=heap&dict=en-ru.regular&flags=231


    // https://translate.yandex.net/api/v1/tr.json/translate?id=e3ad9896.5b31fa49.2f9fd672-51-0&srv=tr-text&lang=en-ru&reason=paste&options=4&text=${phrase}

}