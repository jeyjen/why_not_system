package demo

import demo.util.Config
import demo.util.Util
import demo.util.graphql
import freemarker.cache.ClassTemplateLoader
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.content.default
import io.ktor.content.files
import io.ktor.content.static
import io.ktor.content.staticRootFolder
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.freemarker.FreeMarker
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.Test
import whynot.entity.Contact
import whynot.entity.Contact_type
import whynot.entity.Person
import whynot.entity.WN_account
import java.io.File
import java.security.MessageDigest

fun Application.service(){
    val ctx = App_context(Config.host, Config.port, Config.domain)

    install(ContentNegotiation) {
        jackson {}
    }
    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(Schema::class.java.classLoader, "templates")
    }
    install(CORS){
        host("*")
        header("wn-email")
        header("wn-secret")
        header("wn-ucode")
    }
    routing {
        route("/auth"){
            get("/account"){
                val ucode = call.request.headers["wn-ucode"]?:""
                val email = (call.request.headers["wn-email"]?:"").trim().toLowerCase()
                if(ucode.isNullOrEmpty() && email.isNullOrEmpty()){
                    call.respond(HttpStatusCode.NotFound, "")
                    return@get
                }
                var a: WN_account? = null
                if(ucode.isNullOrEmpty()){
                    a = ctx.user_pvd.wn_accounts(logins = listOf(email)).firstOrNull()
                }
                else{
                    a = ctx.user_pvd.wn_accounts(ids = listOf(ucode)).firstOrNull()
                }
                if(a == null){
                    call.respond(HttpStatusCode.NotFound, "")
                    return@get
                }
                call.respond("")
            }
            route("/email"){
                get("/"){
                    val email = (call.request.headers["wn-email"]?:"").trim().toLowerCase()
                    val secret = call.request.headers["wn-secret"]?:""

                    if(email.isNullOrEmpty() || secret.isNullOrEmpty()){
                        call.respond(HttpStatusCode.Unauthorized, "")
                        return@get
                    }
                    val contact = ctx.user_pvd.contact(Contact_type.email, email)

                    var ucode = ""
                    val secret_hash = Util.sha_256(secret)

                    if(contact != null){
                        var account = ctx.user_pvd.wn_accounts(person_ids = listOf(contact.m_person)).firstOrNull()
                        if(account == null){
                            val a = WN_account()
                            a.id = ctx.user_pvd.get_id()
                            a.login = email
                            a.password = secret_hash
                            a.m_person = contact.m_person
                            ctx.user_pvd.save(listOf(a))
                            ucode = a.id
                        }
                        else{
                            if(account.password.isNullOrEmpty()){
                                account.password = secret_hash
                                ucode = ctx.user_pvd.save(listOf(account)).first()
                            }
                            else{
                                if(account.password.equals(secret_hash)){
                                    ucode = account.id
                                }
                                else{
                                    call.respond(HttpStatusCode.Unauthorized, "")
                                    return@get
                                }
                            }
                        }
                    }
                    else{
                        call.respond(HttpStatusCode.Unauthorized, "")
                        return@get
//                        val p = Person()
//                        p.id = ctx.user_pvd.get_id()
//
//                        val a = WN_account()
//                        a.id = ctx.user_pvd.get_id()
//                        a.login = email
//                        a.password = secret_hash
//                        a.m_person = p.id
//
//                        val c = Contact()
//                        c.id = ctx.user_pvd.get_id()
//                        c.type = Contact_type.email
//                        c.value = email
//                        c.confirm_value = ctx.user_pvd.get_id()
//                        c.m_person = p.id
//
//                        ucode = ctx.user_pvd.save(listOf(a, p, c))
//                                .first()
//                        ctx.email_pvd.confirm_email_send(email, c.confirm_value)
                    }
                    val res = Util.om.createObjectNode()
                    res.put("ucode", ucode)
                    call.respond(res)
                }
                get("/confirm"){
                    val code = call.request.queryParameters["code"]?:""
                    var message = "Некорректный URL для подтверждения"
                    if(!code.isNullOrEmpty()){
                        val c = ctx.user_pvd.contact(code)
                        if(c != null){
                            c.confirm_value = ""
                            c.confirmed = true
                            ctx.user_pvd.save(listOf(c))
                            message = "Email <b>${c.value}</b> успешно подтвержден"
                        }
                    }
                    call.respond(FreeMarkerContent("confirm.ftl", mapOf("message" to message), "e"))
                }
                post("/confirm"){
                    /*
                        По коду аккаунта получен login
                        По логину получен объект контакта
                        Обновлен код подтверждения
                        Отправлен email на почту
                    */

                    val ucode = call.request.headers["wn-ucode"]?:""
                    if(ucode.isNullOrEmpty()){
                        call.respond(HttpStatusCode.Unauthorized, "")
                        return@post
                    }

                    val a = ctx.user_pvd.wn_accounts(ids = listOf(ucode)).firstOrNull()
                    if(a == null){
                        call.respond(HttpStatusCode.Unauthorized, "")
                        return@post
                    }
                    else{
                        val c = ctx.user_pvd.contact(Contact_type.email, a.login)
                        if(c != null){
                            c.confirmed = false
                            c.confirm_value = ctx.user_pvd.get_id()
                            ctx.user_pvd.save(c)
                            ctx.email_pvd.confirm_email_send(c.value, c.confirm_value)
                        }
                        else{
                            call.respond(HttpStatusCode.Unauthorized, "")
                            return@post
                        }
                    }
                    call.respond("ok")
                }
            }


//            get("/vk"){
//                var code = call.request.queryParameters["code"]?:""
//                if(code == ""){
//                    call.respondRedirect("/auth?error=\"access denied\"", permanent = false)
//                }
//                else{
//                    // запомнить пользователя
//
//
//                    val t1 = Clock.systemUTC().instant()
//                    val t2 = t1.plus(100, ChronoUnit.DAYS)
//
//                    var host = "http://why-not.space"
//
//                    var data = Util.http.get<JsonNode>("https://oauth.vk.com/access_token?client_id=6691682&client_secret=9AYw1FA4QCOMXwq6BFiO&redirect_uri=${host}/auth/vk/&code=${code}")
//                    //val token = data.get("access_token")
//                    println(data.toString())
//
//                    val vk_id = data?.get("user_id").asText()
//                    val email = data?.get("email")?.asText()
//
//                    val emails = mutableListOf<String>()
//                    if(email != null){
//                        emails.add(email)
//                    }
//
//                    val cs = client_pvd.users(vk = listOf(vk_id), email = emails)
//
//                    var ucode = ""
//                    if(cs.size > 0){
//                        // установить access_token
//                        ucode = cs.get(0)._id
//                    }
//                    else{
//                        val obj = Util.om.createObjectNode()
//                        obj.put("m_type", "User")
//                        obj.put("firstname", "")
//                        obj.put("lastname", "")
//                        obj.put("middlename", "")
//                        obj.put("email", email)
//                        obj.put("vk", vk_id)
//                        client_pvd.save_objs(obj)
//
//                        val cs = client_pvd.users(vk = listOf(vk_id), email = emails)
//                        ucode = cs.get(0)._id
//                    }
//
//                    call.response.cookies.append("ucode", ucode, CookieEncoding.RAW, path = "/", expires = t2)
//                    call.respondRedirect("/profile/#/", permanent = false)
//                }
//            }
        }
        graphql("/api", Schema(ctx))

        static ("/"){
            staticRootFolder = File("./static/auth/")

            files(File("./"))
            default("index.html")


        }
        static ("/static"){
            staticRootFolder = File("./static/")
            files(File("./"))
        }
        static("/profile"){
            staticRootFolder = File("./static/profile/")
            static ("/static"){
                files(File("./static/"))
            }
            default("index.html")
        }
        static("/lom"){
            staticRootFolder = File("./static/lom/")
            static ("/static"){
                files(File("./static/"))
            }
            default("index.html")
        }
    }
}

class test{
    @Test
    fun auth_email(){
        Config.set("http://localhost", "5984", "http://localhost:7878")
        withTestApplication(Application::service) {
            val call = handleRequest(HttpMethod.Get, "/auth/email") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                addHeader("wn-email", "novikov.eugeniy@gmail.com")
                addHeader("wn-secret", "roulin")
            }
            with(call){
                var data = response.content
                print(data)
            }
        }
    }
    @Test
    fun confirm_email(){
        Config.set("http://localhost", "5984", "http://localhost")
        withTestApplication(Application::service) {
            val call = handleRequest(HttpMethod.Get, "/auth/email/confirm?code=123") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            }
            with(call){
                var data = response.content
                print(data)
            }
        }
    }

    /*
    * http://localhost/auth/email/confirm?code=123
    * */

}