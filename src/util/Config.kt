package demo.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import io.ktor.client.HttpClient
import io.ktor.client.call.TypeInfo
import io.ktor.client.engine.config
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.JsonSerializer
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.content.OutgoingContent
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

class Config{
    companion object {
        var host: String = ""
        var port: String = ""
        var domain: String = ""
        fun set(h: String, p: String, d: String){
            host = h
            port = p
            domain = d
        }
    }
}