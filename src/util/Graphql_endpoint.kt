package demo.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.pipeline.PipelineContext
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import com.google.gson.JsonSyntaxException
import graphql.ExecutionInput
import graphql.ExecutionResult
import graphql.GraphQL
import io.ktor.request.contentType
import io.ktor.request.receiveText


class Graphql_endpoint(val graphql: Graphql_provider) {
    private val _om = ObjectMapper()
    @Throws(InvalidRequestFormatException::class)
    suspend fun process_call(call: ApplicationCall): String {
        val request = try {
            parse_call(call)
        } catch (e: JsonSyntaxException) {
            throw InvalidRequestFormatException("GraphQL Request could not be parsed.", e)
        }
        return graphql
                .execute(request)
                .to_json()
    }

    private fun ExecutionResult.to_json() = _om
            .valueToTree<JsonNode>(this.toSpecification())
            .toString()

    abstract class Graphql_provider{
        abstract val graphQL: GraphQL
        abstract fun SDL(): String
        fun execute(request: Request): ExecutionResult{
            return graphQL.execute(ExecutionInput(request.query, request.operationName, null, "Root", request.variables ))
        }
    }


    @Throws(JsonSyntaxException::class)
    suspend fun parse_call(call: ApplicationCall): Request {
        val contentType = call.request.contentType().contentSubtype
        return when {
        // выборка из url запроса
            call.parameters["query"] != null -> request_url(call)
        // выборка "чистого запроса из тела сообщения"
            contentType == "graphql" -> Request(call.receiveText())
        // выборка запроса из тела сообщения в формате json
            else -> from_json(call.receiveText())
        }
    }

    @Throws(JsonSyntaxException::class)
    private fun request_url(call: ApplicationCall): Request {
        val query = call.parameters["query"]!!
        val operationName= call.parameters["operationName"]
        val variables = call.parameters["variables"]?.let { param ->
            from_json<Map<String, Any>>(param)
        }
        return Request(query, operationName, variables)
    }
    private inline fun <reified T> from_json(json: String):T{
        val tree = _om.readTree(json)
        return _om.treeToValue(tree, T::class.java)
    }
}
data class Request( val query: String? = null, val operationName: String? = null,val variables: Map<String, Any>? = null)

class InvalidRequestFormatException(message: String, cause: Throwable? = null) : Exception(message, cause)
@Throws(JsonSyntaxException::class)

fun Route.graphql(route: String = "/api", provider: Graphql_endpoint.Graphql_provider){
    val endpoint = Graphql_endpoint(provider)
    get(route) { process(endpoint) }
    post(route) { process(endpoint) }
    get("$route/schema"){ retrieve_SDL(endpoint)}
}
suspend fun PipelineContext<Unit, ApplicationCall>.process(endpoint: Graphql_endpoint) {
    val result = try {
        endpoint.process_call(call)
    } catch (e: InvalidRequestFormatException) {
        return call.respond(HttpStatusCode.BadRequest, "The GraphQL query could not be parsed")
    }
    return call.respondText(result, ContentType.Application.Json)
}
suspend fun PipelineContext<Unit, ApplicationCall>.retrieve_SDL(endpoint: Graphql_endpoint) {
    val result = try {
        val res = ObjectMapper().createObjectNode()
        res.put("schema", endpoint.graphql.SDL())
    } catch (e: InvalidRequestFormatException) {
        return call.respond(HttpStatusCode.BadRequest, "The GraphQL query could not be parsed")
    }

    return call.respond(HttpStatusCode.OK, result)
}

