package demo.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import io.ktor.client.HttpClient
import io.ktor.client.call.TypeInfo
import io.ktor.client.engine.config
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.JsonSerializer
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.content.OutgoingContent
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class Util{
    companion object {
        val date_format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val om = ObjectMapper()
        private val digest = MessageDigest.getInstance("SHA-256")
        val http = HttpClient(io.ktor.client.engine.apache.Apache.config {
            customizeClient {
                //setProxy(HttpHost("127.0.0.1", 8888))
                //setMaxConnTotal(1000) // Maximum number of socket connections.
                //setMaxConnPerRoute(100) // Maximum number of requests for a specific endpoint route.
            }
        }) {
            install(JsonFeature) {
                serializer = JacksonSerializer()
            }
        }
        fun keys(vararg arrs: List<Any>): List<ArrayNode>{
            val arr =  mutableListOf<List<Any>>()
            for(a in arrs){
                if(a.size > 0){
                    arr.add(a)
                }
            }
            var idx = MutableList(arr.size){i -> 0}

            val keys = mutableListOf<ArrayNode>()

            var is_upper = false
            do {
                is_upper = false
                var key = Util.om.createArrayNode()
                keys.add(key)
                for (j in 0..arr.size-1) {
                    key.add(Util.om.valueToTree<JsonNode>(arr[j][idx[j]]))
                }

                for (i in idx.size - 1 downTo 0) {
                    var v = idx[i]
                    v++
                    if (v < arr[i].size) {
                        idx[i] = v
                        is_upper = true
                        break
                    } else {

                        for (l in i - 1 downTo 0) {
                            if (idx[l] < arr[l].size - 1) {
                                is_upper = true
                                break
                            }
                        }
                        if (is_upper) {
                            idx[i] = 0
                        }
                        else{
                            break
                        }
                    }
                }
            }while(is_upper)
            return keys
        }

        fun UTC_parse(text: String): OffsetDateTime{
            return Instant.parse(text).atOffset( ZoneOffset.UTC )
        }
        fun UTC_now(): OffsetDateTime{
            return Instant.now().atOffset( ZoneOffset.UTC )
        }
        fun UTC_today(): OffsetDateTime{
            var r = Instant.now().atOffset( ZoneOffset.UTC )
            r = r.minusHours(r.hour.toLong())
            r = r.minusMinutes(r.minute.toLong())
            r = r.minusSeconds(r.second.toLong())
            r = r.minusNanos(r.nano.toLong())
            return r
        }
        fun UTC_tomorrow(): OffsetDateTime{
            var r = Instant.now().atOffset( ZoneOffset.UTC )
            r = r.minusHours(r.hour.toLong())
            r = r.minusMinutes(r.minute.toLong())
            r = r.minusSeconds(r.second.toLong())
            r = r.minusNanos(r.nano.toLong())
            r = r.plusDays(1)
            return r
        }

        fun bytes_to_hex(hash: ByteArray): String {
            val hexString = StringBuffer()
            for (i in hash.indices) {
                val hex = Integer.toHexString(0xff and hash.get(i).toInt())
                if (hex.length == 1) hexString.append('0')
                hexString.append(hex)
            }
            return hexString.toString()
        }
        fun sha_256(value: String):String{
            return Util.bytes_to_hex(digest.digest(value.toByteArray(Charsets.UTF_8)))
        }

    }


}
fun OffsetDateTime.frt(): String{
    return this.format(Util.date_format)
}
fun OffsetDateTime.clear_time(): OffsetDateTime{
    var r = this.minusHours(this.hour.toLong())
    r = r.minusMinutes(r.minute.toLong())
    r = r.minusSeconds(r.second.toLong())
    r = r.minusNanos(r.nano.toLong())
    r = r.plusDays(1)
    return r
}


class JacksonSerializer : JsonSerializer {
    private  val backend = ObjectMapper()
    override suspend fun read(info: TypeInfo, response: HttpResponse): Any {
        return backend.readValue(response.readText(StandardCharsets.UTF_8), info.type.java)
    }
    override fun write(data: Any): OutgoingContent {
        return TextContent(backend.writeValueAsString(data), ContentType.Application.Json)
    }
}

fun String.splitPair(ch: Char): Pair<String, String>? = indexOf(ch).let { idx ->
    when (idx) {
        -1 -> null
        else -> Pair(take(idx), drop(idx + 1))
    }
}