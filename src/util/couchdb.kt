package demo.api.couchdb

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ObjectNode
import whynot.entity.Contact
import demo.util.JacksonSerializer
import demo.util.Util
import io.ktor.client.HttpClient
import io.ktor.client.engine.config
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.*
import io.ktor.http.HttpMethod
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test
import java.net.URLEncoder
import java.util.*

open class couchdb(val db_name: String, val endpoint: String){

    private val _client = HttpClient(io.ktor.client.engine.apache.Apache.config {
        customizeClient {
            //setProxy(HttpHost("127.0.0.1", 8888))
            //setMaxConnTotal(1000) // Maximum number of socket connections.
            //setMaxConnPerRoute(100) // Maximum number of requests for a specific endpoint route.
        }
    }) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }
    private suspend fun request(verb: HttpMethod, url: String, content: JsonNode? = null): JsonNode {

        println("$endpoint$url")
        var res = try {
            _client.request<JsonNode> {
                url("$endpoint$url")
                method = verb
                header("Content-Type", "application/json; charset=utf-8")
                //header("Authorization", "Basic ZW5nbGlzaHByb2ZpbGU6dm9jYWJ1bGFyeQ==")
                //header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                body = content?: ""
            }
        } catch (e: Exception) {
            Util.om.valueToTree<JsonNode>(e)
        }
        return res
    }

    enum class stale{
        ok,
        update_after
    }

    suspend fun ids(count: Int = 1): List<String>{
        val res = request(HttpMethod.Get, "/_uuids?count=$count")
        return res.get("uuids").map { it.asText() }
    }
    val _id_cash = mutableListOf<String>()
    suspend fun get_id(): String{
        if(_id_cash.isEmpty()){
            _id_cash.addAll(ids(500))
        }
        return _id_cash.removeAt(_id_cash.size - 1)
    }

    class view(design: String, name: String, provider: couchdb){
        private val _design = design
        private val _name = name
        private val _provider = provider
        private val _queries = Util.om.createArrayNode()

        fun arguments(
                group:Boolean? = null,
                group_level: Int? = null,
                reduce: Boolean? = null,
                include_docs: Boolean? = null,
                startkey: JsonNode? = null,
                startkey_docid: String? = null,
                endkey: JsonNode? = null,
                endkey_docid: String? = null,
                inclusive_end: Boolean? = null,
                key: JsonNode? = null,
                limit: Long? = null,
                skip: Long? = null,
                descending: Boolean? = null,
                stale: stale? = null,
                keys: ArrayNode? = null
        ): view {

            val opt = Util.om.createObjectNode()
            if(group != null)
                opt.put("group", group)
            if(group_level != null)
                opt.put("group_level", group_level)
            if(reduce != null)
                opt.put("reduce", reduce)
            if(include_docs != null)
                opt.put("include_docs", include_docs)
            if(startkey != null && !(startkey is NullNode))
                opt.set("startkey", startkey)
            if(startkey_docid != null)
                opt.put("startkey_docid", startkey_docid)
            if(endkey != null && !(endkey is NullNode))
                opt.set("endkey", endkey)
            if(endkey_docid != null)
                opt.put("endkey_docid", endkey_docid)
            if(inclusive_end != null)
                opt.put("inclusive_end", inclusive_end)
            if(key != null  && !(key is NullNode))
                opt.set("key", key)
            if(limit != null)
                opt.put("limit", limit)
            if(skip != null)
                opt.put("skip", skip)
            if(descending != null)
                opt.put("descending", descending)
            if(stale != null)
                opt.put("stale", stale.toString())
            if(keys != null)
                opt.set("keys", Util.om.valueToTree(keys))

            _queries.add(opt)
            return this
        }
        suspend fun retrieve(): JsonNode{
            val req = Util.om.createObjectNode()
            req.set("queries", _queries)
            return _provider.request(HttpMethod.Post, "/${_provider.db_name}/_design/$_design/_view/$_name", req)
        }
    }
    fun view(
            design: String,
            name: String
    ): view{
        return couchdb.view(design, name,this)
    }

    suspend fun find(
            selector: Any? = null,
            limit: Long? = null,
            skip: Long? = null,
            sort: Any? = null,
            fields: Collection<String>? = null,
            use_index: String? = null,
            r: Long? = null,
            bookmark: String? = null,
            update: Boolean? = null,
            stable: Boolean? = null,
            stale: stale? = null,
            execution_stats: Boolean? = null
    ): JsonNode{
        val req = Util.om.createObjectNode()
        if(selector != null)
            req.set("selector", Util.om.valueToTree(selector))
        if(limit != null)
            req.put("limit", limit)
        if(skip != null)
            req.put("skip", skip)
        if(sort != null)
            req.set("sort",  Util.om.valueToTree(sort))
        if(fields != null)
            req.set("fields", Util.om.valueToTree(fields))
        if(use_index != null)
            req.put("use_index", use_index)
        if(r != null)
            req.put("r", r)
        if(bookmark != null)
            req.put("bookmark", bookmark)
        if(update != null)
            req.put("update", update)
        if(stable != null)
            req.put("stable", stable)
        if(stale != null)
            req.put("stale", stale.toString())
        if(execution_stats != null)
            req.put("execution_stats", execution_stats)
        return request(HttpMethod.Post, "/$db_name/_find", req)
    }
    suspend fun list(
            design: String,
            list: String,
            view: String,
            selector: ObjectNode? = null,
            sort: ObjectNode? = null,
            group:Boolean? = null,
            group_level: Int? = null,
            reduce: Boolean? = null,
            include_docs: Boolean? = null,
            startkey: JsonNode? = null,
            startkey_docid: String? = null,
            endkey: JsonNode? = null,
            endkey_docid: String? = null,
            inclusive_end: Boolean? = null,
            key: JsonNode? = null,
            limit: Long? = null,
            skip: Long? = null,
            descending: Boolean? = null,
            stale: stale? = null,
            keys: ArrayNode? = null): JsonNode{
        val opt = StringBuilder()
        opt.append("/${db_name}/_design/${design}/_list/${list}/${view}?")
        opt.append("p=p")
        if(group != null)
            opt.append("&group=",group)
        if(group_level != null)
            opt.append("&group_level=", group_level)
        if(reduce != null)
            opt.append("&reduce=", reduce)
        if(include_docs != null)
            opt.append("&include_docs=", include_docs)
        if(startkey != null && !(startkey is NullNode))
            opt.append("&startkey=", encode(startkey.toString()))
        if(startkey_docid != null)
            opt.append("&startkey_docid=", startkey_docid)
        if(endkey != null && !(endkey is NullNode))
            opt.append("&endkey=", encode(endkey.toString()))
        if(endkey_docid != null)
            opt.append("&endkey_docid=", endkey_docid)
        if(inclusive_end != null)
            opt.append("&inclusive_end=", inclusive_end)
        if(key != null  && !(key is NullNode))
            opt.append("&key=", encode(key.toString()))
        if(limit != null)
            opt.append("&limit=", limit)
        if(skip != null)
            opt.append("&skip=", skip)
        if(descending != null)
            opt.append("&descending=", descending)
        if(stale != null)
            opt.append("&stale=", encode(stale.toString()))
        if(keys != null)
            opt.append("&keys=", encode(keys.toString()))
        if(selector != null){
            opt.append("&selector=", encode(selector.toString()))
        }
        if(sort != null){
            opt.append("&sort=", encode(sort.toString()))
        }
        return  request(HttpMethod.Get, opt.toString())
    }

    private fun encode(text: String):String{
        return URLEncoder.encode(text.replace("%", "%25"), "UTF-8")
    }
    suspend fun save_objs(objs: List<ObjectNode>): List<String>{

        var without_ids = mutableListOf<ObjectNode>()
        val keys = Util.om.createArrayNode()
        val mutations_map = mutableMapOf<String, MutableList<ObjectNode>>()
        var docs = Util.om.createArrayNode()

        // sort objects by with or without id
        // and group mutations of object
        for(obj in objs){
            val id = obj.get("_id")?.asText()?:""
            if(id.isNullOrEmpty()){
                without_ids.add(obj)
            }
            else{
                keys.add(id)
                var list = mutations_map.get(id)
                if(list == null){
                    list = mutableListOf<ObjectNode>()
                    mutations_map.put(id, list)
                }
                list.add(obj)
            }
        }

        val res = all_docs(keys, include_docs = true)
        val rows = res.get("rows") as ArrayNode

        val actual_obj_map = mutableMapOf<String, ObjectNode>()

        // sort no existing objects
        for(r in rows){
            if(! r.has("error")){
                val id = r.get("key").asText()
                val doc = r.get("doc") as ObjectNode
                actual_obj_map.put(id, doc)
            }
        }

        // merge object states
        for(m in mutations_map){
            val mutations = m.value
            val act = actual_obj_map.get(m.key)
            var doc = mutations.get(0)
            if(act != null){
                doc = act
            }

            for(mutation in mutations){
                doc = merge(doc, mutation)
                docs.add(doc)
            }
        }

        // add objects without ids
        if(without_ids.size > 0){
            var ids = ids(without_ids.size)
            for (i in 0 .. without_ids.size - 1){
                val doc = without_ids.get(i)
                doc.put("_id", ids.get(i))
                docs.add(doc)
            }
        }
        val update_data = put(docs)
        return update_data.map { it.get("id").asText() }
    }

    private fun merge(doc: ObjectNode, mutation: ObjectNode):ObjectNode{
        val stack = Stack<Pair<ObjectNode, ObjectNode>>()
        stack.push(Pair(mutation, doc))
        while(stack.size > 0){
            val p = stack.pop()
            val fs = p.first.fieldNames().asSequence()
            for (f in fs){
                val v = p.first.get(f)
                if(v is ObjectNode){
                    var sf = p.second.get(f)
                    if(sf == null){
                        sf = p.second.putObject(f)
                    }
                    stack.push(Pair(v , sf as ObjectNode))
                }
                else{
                    p.second.set(f, v)
                }
            }
        }
        return doc
    }

    suspend fun save(entity: Any): String{
        return save(listOf(entity)).firstOrNull()?:""
    }
    suspend fun save(entities: List<Any>): List<String>{
        val objs = mutableListOf<ObjectNode>()
        for (i in entities){
            val obj = Util.om.valueToTree<ObjectNode>(i)
            obj.put("m_type", i.javaClass.simpleName)
            objs.add(obj)
        }
        return  save_objs(objs)
    }

    suspend fun put(objs: ArrayNode): JsonNode{
        val req =  Util.om.createObjectNode()
        //val docs:ArrayNode  = Util.om.valueToTree(objs)
        req.set("docs", objs)
        return request(HttpMethod.Post, "/$db_name/_bulk_docs", req)
    }
    suspend fun design(include_docs: Boolean = false): JsonNode{
        return request(HttpMethod.Get, "/$db_name/_design_docs?include_docs=$include_docs")
    }
    suspend fun index_create(
            name: String,
            fields: Collection<String>,
            type: String = "json",
            selector: ObjectNode? = null,
            sort: ArrayNode? = null,
            limit: Long? = null,
            skip: Long? = null
            ): JsonNode{
        val req = Util.om.createObjectNode()
        req.put("name", name)
        req.put("type", type)
        val index = req.putObject("index")
        index.set("fields", Util.om.valueToTree(fields))
        if(selector != null)
            index.set("selector", selector)
        if(sort != null)
            index.set("sort", sort)
        if(limit != null)
            index.put("limit", limit)
        if(skip != null)
            index.put("sort", skip)

        return request(HttpMethod.Post, "/$db_name/_index", req)
    }

    suspend fun all_docs( keys: ArrayNode? = null, include_docs: Boolean = false): JsonNode{
        val req = Util.om.createObjectNode()
        if(keys != null)
            req.set("keys", keys)
        return request(HttpMethod.Post, "/${db_name}/_all_docs?include_docs=$include_docs", req)
    }


    suspend fun delete(keys: Collection<String>){
        val objs = mutableListOf<ObjectNode>()

        for(key in keys){
            val obj = Util.om.createObjectNode()
            obj.put("_id", key)
            obj.put("_deleted", true)
        }
        save_objs(objs)
//        val res = view(include_docs = true, keys = keys)
//                .retrieve(design, view)
//        val rows = res.get("results")?.get(0)?.get("rows") as ArrayNode
//        rows?.let{
//            for(r in rows){
//                val doc = r.get("doc") as ObjectNode
//                doc.put("_deleted", true)
//            }
//        }

    }
}

class Couchdb_tests{
    @Test
    fun save_entities(){
        val c = couchdb("user", "http://localhost:5984")
        val res = runBlocking { c.save(listOf(Contact())) }
        print(res)
    }

    @Test
    fun t1(){
        runBlocking{
            val c = couchdb("user", "http://localhost:5984")
            val om = ObjectMapper()
            val objs = mutableListOf<ObjectNode>()
            val obj = om.createObjectNode()
            obj.put("_id", "mittens0")
            obj.put("edu", "high")

            objs.add(obj)

            val obj2 = om.createObjectNode()
            obj2.put("_id", "mittens2")
            obj2.put("edu", "high")
            val address = obj2.putObject("address")
            address.put("region", "saratov2")
            objs.add(obj2)
            c.save_objs(objs)
            //print(s.execute(Request("{find{...on Word{_id, freq_score}}}")).toSpecification())
        }
    }

    @Test
    fun t2()= runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        c.delete(listOf("mittens8"))
        //print(s.execute(Request("{find{...on Word{_id, freq_score}}}")).toSpecification())
    }
    @Test
    fun uuids()= runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        print(c.ids(1000))
    }
    @Test
    fun multiview()= runBlocking{
        val c = couchdb("train", "http://localhost:5984")

        val sk1 = Util.om.createArrayNode()
        sk1.add("type")
        sk1.add("key1")
        sk1.add("v1")


//        val ek1 = Util.om.createArrayNode()
//        ek1.add("02271c3ef15f43b3ae62fffc1c015fde")
//        ek1.addObject()


        print(c.view("app", "key")
                //.arguments(group = true)
                .arguments(key =  sk1)
                .retrieve())

    }
    @Test
    fun list()= runBlocking{
        val c = couchdb("lex", "http://localhost:5984")

        val s = Util.om.createObjectNode()
        s.put("pos", "noun")

        val key = Util.om.createArrayNode()
        key.add("A1")
        key.add("animals")

//        val ek1 = Util.om.createArrayNode()
//        ek1.add("02271c3ef15f43b3ae62fffc1c015fde")
//        ek1.addObject()


        print(c.list("app", "filter", "idx_level_topic",
                include_docs = true,
                reduce = false,
                selector = s,
                key = key
                ))
    }
}
