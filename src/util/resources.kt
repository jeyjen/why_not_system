package demo.util

import java.util.ArrayList
import java.util.regex.Pattern
import com.google.common.io.MoreFiles.listFiles

import java.io.*
import java.util.zip.ZipEntry
import java.util.Enumeration
import java.util.zip.ZipException
import java.util.zip.ZipFile



class dd {

    fun getResources(
            query: String): Collection<String> {
        val pattern = Pattern.compile(query)
        val retval = ArrayList<String>()
        val classPath = System.getProperty("java.class.path", ".")
        val classPathElements = classPath.split(System.getProperty("path.separator").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (element in classPathElements) {
            retval.addAll(getResources(element, pattern))
        }
        return retval
    }

    private fun getResources(
            element: String,
            pattern: Pattern): Collection<String> {
        val retval = ArrayList<String>()
        val file = File(element)
        if (file.isDirectory()) {
            retval.addAll(getResourcesFromDirectory(file, pattern))
        } else {
            retval.addAll(getResourcesFromJarFile(file, pattern))
        }
        return retval
    }

    private fun getResourcesFromJarFile(
            file: File,
            pattern: Pattern): Collection<String> {
        val retval = ArrayList<String>()
        val zf: ZipFile
        try {
            zf = ZipFile(file)
        } catch (e: ZipException) {
            throw Error(e)
        } catch (e: IOException) {
            throw Error(e)
        }

        val e = zf.entries()
        while (e.hasMoreElements()) {
            val ze = e.nextElement() as ZipEntry
            val fileName = ze.name
            val accept = pattern.matcher(fileName).matches()
            if (accept) {
                retval.add(fileName)
            }
        }
        try {
            zf.close()
        } catch (e1: IOException) {
            throw Error(e1)
        }

        return retval
    }

    private fun getResourcesFromDirectory(
            directory: File,
            pattern: Pattern): Collection<String> {
        val retval = ArrayList<String>()
        val fileList = directory.listFiles()
        for (file in fileList) {
            if (file.isDirectory()) {
                retval.addAll(getResourcesFromDirectory(file, pattern))
            } else {
                try {
                    val fileName = file.getCanonicalPath()
                    val accept = pattern.matcher(fileName).matches()
                    if (accept) {
                        retval.add(fileName)
                    }
                } catch (e: IOException) {
                    throw Error(e)
                }

            }
        }
        return retval
    }

//    @Throws(IOException::class)
//    private fun getResourceFiles(path: String): List<String> {
//        val filenames = ArrayList<String>()
//
//        getResourceAsStream(path).use({ `in` ->
//            BufferedReader(InputStreamReader(`in`)).use { br ->
//                var resource: String
//
//                resource = br.readLine()
//                while (resource != null) {
//                    filenames.add(resource)
//                }
//            }
//        })
//
//        return filenames
//    }
//
//    private fun getResourceAsStream(resource: String): InputStream {
//        val `in` = getContextClassLoader().getResourceAsStream(resource)
//
//        return `in` ?: javaClass.getResourceAsStream(resource)
//    }
//
//    private fun getContextClassLoader(): ClassLoader {
//        return Thread.currentThread().contextClassLoader
//    }
}
