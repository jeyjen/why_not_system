package demo

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.Application
import io.ktor.http.*
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import org.junit.Test as test

class test_api() {

    val om = ObjectMapper()
    @test fun get_idiom_details() = withTestApplication(Application::service) {
        val call = handleRequest(HttpMethod.Post, "/idiom/details") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody("{\"idioms\": [\"milk\", \"sugar\"], \"lang\": \"ru\"}")
        }
        with(call){
            var data = response.content
        }
    }
    @test fun get_text_stat() = withTestApplication(Application::service) {
        val om = ObjectMapper()
        val req = om.createObjectNode()
        req.put("text", "Once a card type has been created, every time you add a new note, a card will be created based on that card type. Card types make it easy to keep the formatting of your cards consistent and can greatly reduce the amount of effort involved in adding information. They also mean Anki can ensure related cards don’t appear too close to each other, and they allow you to fix a typing mistake or factual error once and have all the related cards updated at once.")
        req.put("lang", "en")
        val call = handleRequest(HttpMethod.Post, "/text/stats") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(req.toString())
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @test fun get_phrase_generate() = withTestApplication(Application::service) {
        val om = ObjectMapper()
        val req = om.createObjectNode()
        val vars = om.createObjectNode()
        req.put("pattern", "Do @v1 @v2?")
        vars.set("@v1", om.createArrayNode().add("I").add("you").add("we").add("they"))
        vars.set("@v2", om.createArrayNode().add("write").add("sleep"))
        req.set("vars", vars)
        val call = handleRequest(HttpMethod.Post, "/phrase/generate") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(req.toString())
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @test fun graphql() = withTestApplication(Application::service) {
        val call = handleRequest(HttpMethod.Post, "/api") {
            addHeader(HttpHeaders.ContentType, "application/graphql") //ContentType.Application.Json.toString()
            setBody("{all_links(id: \"2\"){url, description}}")
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @test fun graphql_get() = withTestApplication(Application::service) {
        val call = handleRequest(HttpMethod.Get, "/api?query=query atest(\$id: String!){all_links(id: \$id){url, description}}&operationName=atest&variables={\"id\":\"me\"}") {
            //addHeader(HttpHeaders.ContentType, "application/graphql") //ContentType.Application.Json.toString()
            //setBody("{all_links(id: \"2\"){url, description}}")
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @test fun graphql_post() = withTestApplication(Application::service) {
        val req = om.createObjectNode()
        req.put("query", "query atest(\$id: String!){all_links(id: \$id){id, url, description}}")
        req.put("operationName", "atest")
        val vars = om.createObjectNode()
        vars.put("id", "123")
        req.set("variables", vars)

        val call = handleRequest(HttpMethod.Post, "/api") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(req.toString())
        }
        with(call){
            var data = response.content
            println(data)
        }
    }

    @test fun couchdb_graphql() = withTestApplication(Application::service) {
        val call = handleRequest(HttpMethod.Post, "/api") {
            addHeader(HttpHeaders.ContentType, "application/graphql") //ContentType.Application.Json.toString()
            setBody("{view(design: \"app\", name: \"nn\"){total_rows, rows{key, value}}}")
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @test fun couchdb_graphql_find() = withTestApplication(Application::service) {
        val call = handleRequest(HttpMethod.Post, "/api") {
            addHeader(HttpHeaders.ContentType, "application/graphql") //ContentType.Application.Json.toString()
            setBody("{allItems{... on Item{id2:id}... on OtherItem{id2:name}}}")
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @test fun graphql_mutation_collection() = withTestApplication(Application::service) {
        val call = handleRequest(HttpMethod.Post, "/api") {
            addHeader(HttpHeaders.ContentType, "application/graphql") //ContentType.Application.Json.toString()
            setBody("mutation ss{ save(objs: [{_id: \"identifier\", name: \"Eugene\", bool: true, nule: null, float: 3.4, int: 33, arr:[\"hello\"], obb:{id: \"\"}}])}")
        }
        with(call){
            var data = response.content
            println(data)
        }
    }



    //query={all_links(id: \"2\"){url, description}}&operationName=&variables=

//    @test fun testRequest() = withTestApplication(Application::testableModule) {
//        with(handleRequest(HttpMethod.Get, "/")) {
//            assertEquals(HttpStatusCode.OK, response.status())
//            assertEquals("Test String", response.content)
//        }
//        with(handleRequest(HttpMethod.Get, "/index.html")) {
//            assertFalse(requestHandled)
//        }
//    }

}