package demo

import com.fasterxml.jackson.databind.ObjectMapper
import demo.api.couchdb.couchdb
//import demo.api.createSchema
import demo.graphql.graphql_demo
import demo.provider.*
import demo.util.lemmatizer
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert
import org.junit.Test as test

class TestSource() {
    @test fun translate_word_test() = runBlocking{
        val y = yandex()
        val res = y.translate_word("milk")
    }
    @test fun translate_phrase_test() = runBlocking {
        val y = yandex()
        val res = y.translate_phrase("milk")
    }


    @test fun mailru_retrieve() {
        var dd = mailru()
        var ddd = dd.retrieve("английский")
    }
    @test fun gis_retrieve(){
        val ga = gis()
        ga.retrieve("Английский язык")
    }
    @test fun phrase_generate(){
        val ga = phrase()
        val vars = mapOf(
                "@v1" to listOf("I", "you", "we", "they"),
                "@v2" to listOf("write", "sleep", "wait"))

        val phs = ga.generate("Do @v1 @v2?", vars)
        for(p in phs){
            println(p)
        }
    }
    @test fun phrase_generate_with_s(){
        val ga = phrase()
        val vars = mapOf(
                "@v1" to listOf("he", "she", "it"),
                "@v2:s" to listOf("quiz", "bus", "tax", "path", "crash", "cherry", "play", "dry", "moo", "buffalo", "do"))

        val phs = ga.generate("@v1 @v2:s.", vars)
        for(p in phs){
            println(p)
        }
    }
    @test fun phrase_generate_with_ing(){
        val ga = phrase()
        val vars = mapOf(
                "@v1" to listOf("he", "she", "it"),
                "@v2:ing" to listOf("cry", "swim", "break", "make", "agree", "lie", "carry", "hit", "open", "signal", "rebel", "traffic"))

        val phs = ga.generate("@v1 is @v2:ing.", vars)
        for(p in phs){
            println(p)
        }
    }
    @test fun end_define_s(){
//        val ga = phrase()
//        val verify = listOf("quizzes", "buses", "taxes", "paths", "crashes", "cherries", "plays", "dries", "buffaloes", "does")
//        var res = ga.define_end("s", listOf("quiz", "bus", "tax", "path", "crash", "cherry", "play", "dry", "moo", "buffalo", "do")).toHashSet()
//        for(v in verify){
//            Assert.assertTrue("Example ${v}",res.contains(v))
//            println(v)
//        }
    }
    @test fun end_define_ing(){
        val ga = phrase()
        val verify = listOf("being", "crying", "having", "swimming", "breaking", "making", "agreeing", "lying", "carrying", "hitting", "opening", "signalling", "rebelling", "trafficking")
        var res = ga.define_end("ing", listOf("be", "cry", "have", "swim", "break", "make", "agree", "lie", "carry", "hit", "open", "signal", "rebel", "traffic")).toHashSet()

        for(v in verify){
            Assert.assertTrue("Example ${v}",res.contains(v))
            println(v)
        }
    }
    @test fun end_define_ed(){
        val ga = phrase()
        val verify = listOf("talked", "answered", "agreed", "peed", "smiled", "dried", "tried", "stayed", "nodded", "preferred", "opened", "flexed", "travelled", "mimicked")
        var res = ga.define_end("ed", listOf("talk", "answer", "agree", "pee", "smile", "dry", "try", "stay", "nod", "prefer", "open", "flex", "travel", "mimic")).toHashSet()
        for(v in verify){
            Assert.assertTrue("Example ${v}",res.contains(v))
            println(v)
        }
    }
    @test fun ru_transcription(){
        val ya = yandex()
        // nəʊ
        // ˈrɛəlɪ
        val res = ya.ru_transcrtiption("kənˈsiːv")
    }
    @test fun lematize_text_test(){
        val l = lemmatizer()
        val ls = l.lemmatize("""Most It's am bananas consumed around the world`s are of a type known as the Cavendish, which is vulnerable to a plant pest. Richard Allen, senior conservation assessor at the Royal Botanic Gardens, Kew, said the species (Ensete perrieri) could have in-built tolerance to drought or disease. "It doesn't have Panama disease in it, so perhaps it has genetic traits against the disease," he said We don't know until we actually do research on the banana itself, but we can't do the research until it's saved.""")
        ls.forEach {
            println(it)
        }
    }

    @test fun retrieve_english_profile() = runBlocking{
        val ep = en_profile()
        ep.retrieve()
    }
    @test fun upload_to_db() = runBlocking{
        val ep = en_profile()
        ep.upload()
    }
    @test fun couchdb_view() = runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        val res = c.view(
            startkey = "n2"//,
            //group = true
        )
//                .view(
//                reduce = false,
//                keys = listOf(listOf("B1", "word")))

                .retrieve( "app", "nn")

        println(res)
    }
    @test fun couchdb_find() = runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        val selector = ObjectMapper().createObjectNode()
        selector.put("spelling", "convenient")
        val res = c.find(
            selector = selector
        )

        println(res)
    }
    @test fun couchdb_design() = runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        print( c.design())
    }
    @test fun couchdb_put() = runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        val d1 = ObjectMapper().createObjectNode()
        d1.put("_id", "mittens3")
        d1.put("name", "NAME1")

        val d2 = ObjectMapper().createObjectNode()
        d2.put("name", "NAME2")

        val d3 = ObjectMapper().createObjectNode()
        d3.put("_id", "id_1")
        d3.put("name", "NAME3")

        val arr = ObjectMapper().createArrayNode()
        arr.add(d1)
        arr.add(d2)
        arr.add(d3)
        print(c.put(arr))
    }
    @test fun couchdb_all_docs() = runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        val res = c.all_docs(
            keys = listOf("mittens9", "mittens8", "mittens7", "mittens6")
        )
        println(res)
    }

    @test fun couchdb_delete() = runBlocking{
        val c = couchdb("demo", "http://localhost:5984")
        print( c.delete("app", "ids", listOf("mittens9")))
    }

    @test fun create_test_scheme(){
        //createSchema()
    }
}